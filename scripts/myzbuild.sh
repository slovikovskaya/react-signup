#!/usr/bin/env bash

DIR=`pwd`

mv ${DIR}/src/translations/it.json ${DIR}/src/translations/it_bak.json
mv ${DIR}/src/translations/it_prod.json ${DIR}/src/translations/it.json

REACT_APP_STAGE=production react-scripts build

chmod 777 ${DIR}/build/static/js/*
for file in ${DIR}/build/static/js/*.js; do
  cp "$file" ${DIR}/../../public/js/signup.bundle.js
done

chmod 777 ${DIR}/build/static/css/*
for file in ${DIR}/build/static/css/*.css; do
  cp "$file" ${DIR}/../../public/js/signup.bundle.css
done

mv ${DIR}/src/translations/it.json ${DIR}/src/translations/it_prod.json
mv ${DIR}/src/translations/it_bak.json ${DIR}/src/translations/it.json