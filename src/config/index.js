const dev = {
    baseUrl: 'http://my.zanichelli.local/',
    cdn: 'http://cdn-my.zanichelli.local'
};

const prod = {
    baseUrl: 'myZanichelli.signup.base',
    cdn: 'myZanichelli.signup.cdn'
};

const config = process.env.REACT_APP_STAGE === 'production'
    ? prod
    : dev;

export default {
    ...config
};