import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import store from "./store/index";
import App from "./components/App";
import './App.css'
import {IntlProvider} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_it from 'react-intl/locale-data/it';

import messages_it from "./translations/it.json";

const messages = {
    'it': messages_it
};
const language = navigator.language.split(/[-_]/)[0];  // language without region code

addLocaleData([...locale_it]);

render(
    <IntlProvider locale={language} messages={messages[language]}>
        <Provider store={store}>
            <App />
        </Provider>
    </IntlProvider>,
    document.getElementById("root")
);
