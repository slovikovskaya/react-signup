import React, {Component} from 'react';
import ReactPopup from "reactjs-popup";
import {FormattedMessage} from 'react-intl';

class Popup extends Component {
    renderTrigger = () => {
        return (
            <span>
                <a className="question popup-opener">question</a>
                <a id="popupStato" className="popup-opener text-danger"><FormattedMessage id="#i18n:Registration.State#"/></a>
            </span>
        )
    }

    render = () => {

        return (
            <ReactPopup position={'right center'} contentStyle={{background:'none', border:'none', boxShadow:'none'}}
                   arrow={false}
                   trigger={this.renderTrigger()}
            >
                {close => (
                        <div className="popup pop-up-province-position show">
                            <a className="close" onClick={close}/>
                            {this.props.children}
                        </div>
                )}
            </ReactPopup>
        )
    }
}

export default Popup;