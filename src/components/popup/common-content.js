import React from 'react'
import {FormattedHTMLMessage} from 'react-intl';

export default () => (
    <div>
        <FormattedHTMLMessage id="#i18n:Registration.ElencoStati#"/>
    </div>
);