import React, {Component} from 'react'
import {withFormApi} from 'react-form'
import {FormattedMessage, injectIntl} from 'react-intl';

import {UNIVERSITY_TYPE, SECONDARY_SCHOOL_II, SECONDARY_SCHOOL_I, OTHER_SCHOOL_TYPE} from "../../constants/school-types";
import {DOCENTE_PROFILE, STUDENTE_PROFILE, PROFESSIONISTA_PROFILE} from "../../constants/profiles";
import {TIPO_STUDENTE_MINORENNE} from "../../constants/studente-types";
import {DOCENTE, STUDENTE, PROFESSIONISTA} from "../../constants/user-types";

import Breadcrumbs from "./../breadcrumbs";
import CheckboxInput from "../form/field/checkbox-input";
import SaveButton from "../save-button";
import ParentEmail from "../form/field/email/parent-email";

class Privacy extends Component {

    render = () => {
        let values = this.props.formApi.values,
            schoolTypeId = values['school_type_radio'],
            profileId = values['user_type_radio'],
            profile = null,
            schoolType = null

        switch (profileId){
            case DOCENTE:
                profile = DOCENTE_PROFILE;
                break;
            case STUDENTE:
                profile = STUDENTE_PROFILE;
                break;
            case PROFESSIONISTA:
                profile = PROFESSIONISTA_PROFILE;
                break;
            default:
                break;
        }

        switch (schoolTypeId){
            case "1":
                schoolType = SECONDARY_SCHOOL_I;
                break;
            case "2":
                schoolType = SECONDARY_SCHOOL_II;
                break;
            case "3":
                schoolType = UNIVERSITY_TYPE;
                break;
            default:
                schoolType = OTHER_SCHOOL_TYPE;
                break;
        }

        return (
            <div className="steps-bloks">
                <Breadcrumbs resetErrorSchoolDistrict={this.props.resetErrorSchoolDistrict}
                             resetErrorClasseConcorso={this.props.resetErrorClasseConcorso}
                             resetErrorPrivacy={this.props.resetErrorPrivacy}
                             resetProfession={this.props.resetProfession}
                             resetStudentErrorAteneo={this.props.resetStudentErrorAteneo}
                             resetStudentErrorPrivacy={this.props.resetStudentErrorPrivacy}
                             resetStudenteMaggiorenneErrorPrivacy={this.props.resetStudenteMaggiorenneErrorPrivacy}
                             resetErrorBirthDate={this.props.resetErrorBirthDate}
                             resetErrorPrivacyMinorenne={this.props.resetErrorPrivacyMinorenne}/>
                <div className="step active">
                    <div className="step-holder">
                        { this.props.formApi.values.studente_tipo === TIPO_STUDENTE_MINORENNE ?
                            <ParentEmail name="student_parent_email"
                                         label="#i18n:Registration.ParentEmail.Label#"
                                         placeholder="#i18n:Registration.ParentEmail.Placeholder#" />
                            : null}
                        <div className="checkbox-rows checkbox-text">
                            { this.props.formApi.values.studente_tipo !== TIPO_STUDENTE_MINORENNE ?
                                <CheckboxInput
                                    name="user_privacy"
                                    label="#i18n:Registration.Maggiorenne.InformativaPrivacy#"
                                    error="#i18n:Registration.Privacy.Error#" />
                                : <CheckboxInput
                                    name="user_young_privacy"
                                    label="#i18n:Registration.Minorenne.InformativaPrivacy#"
                                    error="#i18n:Registration.Privacy.Error#" /> }
                            <CheckboxInput
                                name="user_wantActivation"
                                label="#i18n:Registration.WantActivation#"
                                error="#i18n:Registration.validation.WantActivation#"
                            />
                            <CheckboxInput
                                name="user_wantActivation2"
                                label="#i18n:Registration.WantActivation2#"
                                error="#i18n:Registration.validation.WantActivation2#"
                            />
                        </div>
                        {this.props.formApi.values.studente_tipo === TIPO_STUDENTE_MINORENNE ?
                            <div className="text-area">
                                <p><FormattedMessage id="#i18n:Registration.Minorenne.FeedbackMessage#"/></p>
                            </div>
                            : <CheckboxInput
                                name="user_wantNewsletter"
                                label={profile === DOCENTE_PROFILE && schoolType !== UNIVERSITY_TYPE ? "#i18n:Registration.WantNewsletter.SchoolTeacher#" : "#i18n:Registration.WantNewsletter#"}
                                checked="checked" /> }
                        <div className="btn-hold">
                            <SaveButton />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default injectIntl(withFormApi(Privacy));

