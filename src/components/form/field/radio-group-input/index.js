import React, {Component} from 'react'
import {RadioGroup, withFormApi} from 'react-form';
import PropTypes from "prop-types";
import Error from "./../error";
import {injectIntl} from 'react-intl';

class RadioGroupInput extends Component {

    constructor (props) {
        super(props);

        this.state = {
            error: null,
        }
    }

    checkEmpty = (value, error) => {
        if(!value) {
            return this.props.intl.formatMessage({id:"#i18n:Registration.validation.notEmpty.profile#"});
        }

        return null;
    }

    validate = (value) => {
        this.setState({
            error: this.checkEmpty(value)
        })

        return this.state.error
            ? {error: this.state.error, success: null}
            : {error: null, success: 'ok'}
    };

    onChange = (e) => {
        if (this.props.onChange) {
            this.props.onChange(e);
        }

        // workaround necessario perché la validazione non viene fatta onChange ma solo onBlur
        this.props.formApi.setError(this.props.name, null)
    }

    render = () => {
        let {name, ...rest} = this.props
        return (
            <RadioGroup
                {...rest}
                field={name}
                validate={this.validate}
                onChange={this.onChange}
            >
                {this.props.children}
                <Error error={this.state.error}/>
            </RadioGroup>
        )
    }
}

RadioGroupInput.propTypes = {
    name:     PropTypes.string.isRequired,
    tabIndex: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]).isRequired
};

export default injectIntl(withFormApi(RadioGroupInput))