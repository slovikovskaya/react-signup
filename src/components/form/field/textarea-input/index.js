import React from 'react'
import {TextArea} from 'react-form'
import {withFormApi} from 'react-form';
import {FormattedMessage, injectIntl} from 'react-intl'
import {TextInput_} from '../text-input'

class TextAreaInput extends TextInput_ {

    getStatusClass = () => {
        let name        = this.props.name;
        let statusClass = 'form-group full-width'
        if (this.props.validate && this.props.formApi.touched[name]) {
            statusClass += (this.state.errors.length ? 'not-valid' : 'valid')
        }

        return statusClass;
    }

    renderField = () => {
        let {name, label, placeholder, validate, formApi, ...rest} = this.props
        return (
            <div className={this.getStatusClass()}>
                <label htmlFor={name}><FormattedMessage id={label}/></label>
                <TextArea
                    {...rest}
                    id={name}
                    field={name}
                    className="form-control"
                    placeholder={this.formatMessage(placeholder)}
                    validate={validate && this.validate}
                />
                {this.getErrorBlock()}
            </div>
        )
    }
}

TextAreaInput.propTypes = {
    ...TextInput_.propTypes
}

export default injectIntl(withFormApi(TextAreaInput))