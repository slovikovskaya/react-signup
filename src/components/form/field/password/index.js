import {TextInput_} from "./../text-input";
import {withFormApi} from 'react-form';
import {injectIntl} from 'react-intl'

export class Password_ extends TextInput_ {

    validate = (value) => {
        let errors = []

        this.checkEmpty(value, errors)

        this.setState({
            errors: errors
        })

        setTimeout(() => {
            if (this.props.formApi.values['user_password_cfm']) {
                this.props.formApi.validate('user_password_cfm')
            }
        }, 1)

        return this.state.errors.length
            ? { error: this.state.errors.join(', '), success: null }
            : { error: null, success: 'ok' }
    };

    getType = () => (
        'password'
    )
}

const Password = injectIntl(withFormApi(Password_))

export default Password;