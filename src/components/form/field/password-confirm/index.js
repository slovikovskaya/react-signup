import {Password_} from "./../password";
import {withFormApi} from 'react-form';
import {injectIntl} from 'react-intl';

class PasswordConfirm extends Password_ {

    checkEqual = (value, errors) => {
        if(value !== this.props.formApi.values.user_password) {
            errors.push(this.formatMessage("#i18n:Registration.validation.user_password_cfm#"))
        }
    }

    validate = (value) => {
        let errors = []

        this.checkEqual(value, errors)

        if (!errors.length) {
            this.checkEmpty(value, errors)
        }

        this.setState({
            errors: errors
        })

        return this.state.errors.length
            ? { error: this.state.errors.join(', '), success: null }
            : { error: null, success: 'ok' }
    };
}

export default injectIntl(withFormApi(PasswordConfirm))