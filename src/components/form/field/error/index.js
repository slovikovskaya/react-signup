import React, {Component} from 'react'

export default class Error extends Component {

    render = () => (
            <small className="help-block validation-error-block">{this.props.error}</small>
        )
}