import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Error from "./../error";
import {Checkbox} from 'react-form';
import {withFormApi} from 'react-form';
import {FormattedHTMLMessage, injectIntl} from 'react-intl';

class CheckboxInput extends Component {

    constructor () {
        super();

        this.state = {
            error: null
        }
    }

    checkEmpty = (value, errors) => {
        if(!value) {
            errors.push(this.props.error);
        }
    }

    validate = (value) => {

        if(this.props.error && this.props.error.length && !value) {
            this.setState({
                error: this.props.error
            })

            return { error: this.props.error, success: null }
        }

        this.setState({
            error: null
        })

        return { error: null, success: 'ok' }
    };

    getErrorBlock = () => (this.state.error && <Error error={this.props.intl.formatMessage({id: this.state.error})}/>);
    
    getStatusClass = () => {
        return 'checkbox not-show-valid-icon ' + (this.state.error? 'not-valid' : 'valid')
    }

    render = () => (
        <div className={this.getStatusClass()}>
            <Checkbox
                id={this.props.name}
                field={this.props.name}
                validate={this.validate}
            />
            <label htmlFor={this.props.name}>
                <FormattedHTMLMessage id={this.props.label}/>
            </label>

            {this.getErrorBlock()}
        </div>
    )
}

CheckboxInput.propTypes = {
    name: PropTypes.string.isRequired,
    validate: PropTypes.func,
    error: PropTypes.string,
};

export default injectIntl(withFormApi(CheckboxInput));