import React from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';
import {CustomField} from '../custom-field/index'
import 'react-select/dist/react-select.css';
import {injectIntl, FormattedMessage} from 'react-intl'

export class Select extends CustomField {

    getContainerClassName = () => {
        let className = "Select-container"
        if (this.props.className) {
            className = className + ' ' + this.props.className
        }
        return className;
    }

    renderLabel = () => {
        if (this.props.label) {
            return (<label className={this.props.labelClass} htmlFor={this.props.name}><FormattedMessage id={this.props.label}/></label>)
        }

        return null;
    };

    renderSelect = () => {
        const {searchPromptText, innerClassName, placeholder, ...rest} = this.props
        return <ReactSelect
            {...rest}
            className={innerClassName}
            value={this.getFieldValues().value || ''}
            searchPromptText={this.formatMessage(searchPromptText)}
            placeholder={this.formatMessage(placeholder)}
            onChange={this.onChange}
            onBlur={this.onBlur}
        />
    }

    render = () => {
        return (
            <div>
                {this.renderLabel()}
                <div className={this.getContainerClassName()}>
                    {this.renderSelect()}
                </div>
                {this.renderValidationMessages()}
            </div>
        )
    }
}

Select.propTypes = {
    ...CustomField.propTypes,
    options:          PropTypes.array,
    innerClassName:   PropTypes.string,
    searchable:       PropTypes.bool,
    searchPromptText: PropTypes.string,
    clearable:        PropTypes.bool,
    multi:            PropTypes.bool,
    disabled:         PropTypes.bool,
};

Select.defaultProps = {
    ...CustomField.defaultProps,
    options:          [],
    searchable:       false,
    searchPromptText: "#i18n:Registration.autocomplete.typeToSearch#",
    clearable:        false,
    multi:            false,
    disabled:         false,
};

export default injectIntl(Select);
