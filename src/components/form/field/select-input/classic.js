import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Select from './object';
import Error from "./../error";
import {FormattedMessage} from 'react-intl';

class SelectInput extends Component {

    constructor() {
        super();

        this.state = {
            options: [],
            error:   null
        }
    }

    validate = (value) => {

        if (!value) {
            this.setState({error: '<FormattedMessage id="#i18n:Registration.validation.notEmpty#"/>'})
            return {error: this.state.error, success: null}
        }

        this.setState({error: null})

        return {error: null, success: 'ok'}
    };

    onChange = (value) => {
        if (this.props.onChange) {
            this.props.onChange(value)
        }
    };

    componentDidMount = () => {
        this.setState({
            options: this.props.options || []
        })
    };

    getLabelBlock = () => {
        if (this.props.label) {
            return (<label className={this.props.labelClass} htmlFor={this.props.name}>{this.props.label}</label>)
        }

        return null;
    };

    render = () => (
        <div>
            {this.getLabelBlock()}
            <Select
                field={this.props.name}
                options={this.state.options}
                onChange={this.onChange}
                placeholder={this.props.placeholder}
                className={this.props.className}
                validate={this.validate}
            />
            <Error error={this.state.error}/>
        </div>
    )
}

SelectInput.propTypes = {
    className:   PropTypes.string,
    placeholder: PropTypes.string,
    onChange:    PropTypes.func,
    label:       PropTypes.string,
    labelClass:  PropTypes.string,
}

export default SelectInput