import {Autocomplete} from './autocomplete';
import {injectIntl} from 'react-intl'

export class SelectAsync extends Autocomplete {}

SelectAsync.defaultProps = {
    ...Autocomplete.defaultProps,
    searchable: false,
    clearable:  false,
    autoload:   true,
};

export default injectIntl(SelectAsync);
