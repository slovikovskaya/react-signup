import React from 'react';
import PropTypes from 'prop-types';
import {Select} from './index';
import ReactSelect from 'react-select'
import {injectIntl} from 'react-intl';

export class Autocomplete extends Select {

    renderSelect = () => {
        const {innerClassName, searchPromptText, placeholder, loadingPlaceholder, getOptions, ...rest} = this.props
        
        return <ReactSelect.Async
            {...rest}
            className={innerClassName}
            value={this.getFieldValues().value || ''}
            searchPromptText={this.formatMessage(searchPromptText)}
            loadOptions={getOptions}
            placeholder={this.formatMessage(placeholder)}
            loadingPlaceholder={this.formatMessage(loadingPlaceholder)}
            onChange={this.onChange}
            onBlur={this.onBlur}
        />
    }
}

Autocomplete.propTypes = {
    ...Select.propTypes,
    getOptions:         PropTypes.func.isRequired,
    autoload:           PropTypes.bool,
    loadingPlaceholder: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
};

Autocomplete.defaultProps = {
    ...Select.defaultProps,
    searchable:         true,
    clearable:          true,
    multi:              false,
    autoload:           false,
    loadingPlaceholder: "#i18n:Registration.autocomplete.loading#"
};

export default injectIntl(Autocomplete);
