import React, {Component} from 'react'
import {withField} from 'react-form'
import {injectIntl} from 'react-intl'

class SelectWrapper extends Component {
    render() {
        const {
                  fieldApi: {value, setValue, setTouched},
                  intl,
                  options,
                  onChange,
                  onBlur,
                  placeholder,
                  ...       rest
              } = this.props

        const resolvedOptions = options.find(option => option.value === '') || placeholder === false
            ? options
            : [
                {
                    label:    placeholder || this.props.intl.formatMessage({id:'#i18n:Registration.SelectOne.Label#'}),
                    value:    '',
                    disabled: true
                },
                ...options
            ]

        const nullIndex     = resolvedOptions.findIndex(option => option.value === '')
        const selectedIndex = resolvedOptions.findIndex(option => value && option.value === value.value)

        return (
            <select
                {...rest}
                value={selectedIndex > -1 ? selectedIndex : nullIndex}
                onChange={e => {
                    const val = resolvedOptions[e.target.value]
                    setValue(val)
                    if (onChange) {
                        onChange(val, e)
                    }
                }}
                onBlur={e => {
                    setTouched()
                    if (onBlur) {
                        onBlur(e)
                    }
                }}
            >
                {resolvedOptions.map((option, i) => (
                    <option key={option.value} value={i} disabled={option.disabled}>
                        {option.label}
                    </option>
                ))}
            </select>
        )
    }
}

const Select = injectIntl(withField(SelectWrapper))

export default Select