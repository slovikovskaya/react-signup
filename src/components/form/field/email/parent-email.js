import {withFormApi} from 'react-form';
import {Email_} from "./../email";
import {TextInput_} from "./../text-input";
import {injectIntl} from 'react-intl';

export class ParentEmail_ extends Email_ {

    constructor () {
        super();

        this.state.note = '#i18n:Registration.ParentEmail.Note#'
    }

    checkEqualToParent = (value, errors) => {
        if(value === this.props.formApi.values.user_email) {
            errors.push(this.formatMessage("#i18n:Registration.ParentEmailEqualsToChildEmail.Error#"))
        }
    }

    asyncValidate = (value) => {}

    validate = (value) => {
        let errors = []

        this.checkEmpty(value, errors)

        if (!errors.length) {
            this.checkEmail(value, errors)
        }

        if (!errors.length) {
            this.checkEqualToParent(value, errors)
        }

        this.setState({
            errors: errors
        })

        return this.state.errors.length
            ? {error: this.state.errors.join(', '), success: null}
            : {error: null, success: 'ok'}
    };

    render = () => (
        this.renderField()
     )
}

ParentEmail_.propTypes = {
    ...TextInput_.propTypes
}

const ParentEmail = injectIntl(withFormApi(ParentEmail_))

export default ParentEmail;