import React from 'react'
import {TextInput_} from "./../text-input";
import {withFormApi} from 'react-form';
import axios from 'axios'
import config from './../../../../config'
import {FormattedMessage, FormattedHTMLMessage, injectIntl} from 'react-intl';

const EXISTING_MAIL_MESSAGE = "#i18n:Registration.validation.existingLogin#"

export class Email_ extends TextInput_ {

    constructor() {
        super();

        this.state = {
            errors:            [],
            asyncErrors:       [],
            note:              "#i18n:Registration.EmailNote#",
            exists:            false,
            validating:        false,
            lastAsyncValid:    null,
            lastAsyncNotValid: null
        }

        this.cancelPendingRequest = null
    }

    setCancelPendingRequest = (c) => {
        this.cancelPendingRequest = c
    }

    checkEmail = (value, errors) => {
        let re    = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let valid = re.test(String(value).toLowerCase());

        if(!valid) {
            errors.push(<FormattedMessage id="#i18n:Registration.validation.email#"/>)
        }

        return valid
    }

    checkExists = (value, asyncErrors) => {
        let setCancelPendingRequest = this.setCancelPendingRequest

        return axios({
            url:             config['baseUrl'] + 'rest/checkExistingLogin?user_email=' + value,
            method:          'get',
            headers:         {
                'Accept': 'application/json'
            },
            responseType:    'json',
            withCredentials: true,
            cancelToken: new axios.CancelToken(function (cancel) {
                setCancelPendingRequest(cancel)
            })
        })
            .then((response) => {

                let exists = false;

                if (response.status === 200) {
                    if (response.data) {
                        if (!response.data.valid) {
                            asyncErrors.push(this.formatMessage(EXISTING_MAIL_MESSAGE))
                            exists = true;
                        }
                    }
                }

                this.setState({
                    asyncErrors:       asyncErrors,
                    exists:            exists,
                    validating:        false,
                    lastAsyncValid:    exists ? this.state.lastAsyncValid : value,
                    lastAsyncNotValid: exists ? value : this.state.lastAsyncNotValid,
                })

                return this.state.asyncErrors.length
                    ? {error: this.state.asyncErrors.join(', '), success: null}
                    : {error: null, success: 'ok'}

            })
    }

    validate = (value) => {
        let errors = []

        this.checkEmpty(value, errors)

        if (!errors.length) {
            this.checkEmail(value, errors)
        }

        this.setState({
            errors: errors,
            exists: false,
        })

        return this.state.errors.length
            ? {error: this.state.errors.join(', '), success: null}
            : {error: null, success: 'ok'}
    };

    asyncValidate = (value) => {
        let notValid = !this.checkEmpty(value, []) || !this.checkEmail(value, []),
            alreadyCheckedNotExisting = value === this.state.lastAsyncValid,
            alreadyCheckedExisting = value === this.state.lastAsyncNotValid

        if (alreadyCheckedNotExisting || notValid) {
            this.setState({
                asyncErrors: [],
                exists:      false,
            })
            return Promise.resolve({error: null, success: 'ok'})
        }

        if (alreadyCheckedExisting) {
            this.setState({
                asyncErrors: [this.formatMessage(EXISTING_MAIL_MESSAGE)],
                exists:      true,
            })
            return Promise.resolve({error: this.formatMessage(EXISTING_MAIL_MESSAGE), success: false})
        }

        if (this.state.validating) {
            this.cancelPendingRequest()
        }

        this.setState({
            asyncErrors: [],
            validating:  true
        })

        return this.checkExists(value, [])
    }

    getStatusClass = () => {
        let {name}                            = this.props,
            {validating, errors, asyncErrors} = this.state,
            statusClass                       = 'form-group',
            valid                             = (errors.length + asyncErrors.length) === 0
        if (this.props.formApi.touched[name]) {
            statusClass += validating ? ' validating' : (valid ? ' valid' : ' not-valid')
        }

        return statusClass;
    }

    renderExistError = () => (this.state.exists
            ? <div>
                <div className="error-block email-error-block after">
                    <div className="error-message-large have-large-icon">
                        <p className="text-danger">Ops! <span><FormattedMessage id="#i18n:Registration.validation.existingLoginError#"/></span></p>
                    </div>
                </div>
                <div className="error-block-secondary email-error-block">
                    <p><FormattedHTMLMessage id="#i18n:Registration.validation.existingLoginSecondary#"/>
                    </p>
                </div>
            </div>
            : null
    )

    render = () => (
        <div>
            {this.renderField()}
            {this.renderExistError()}
        </div>
    )
}

Email_.propTypes = {
    ...TextInput_.propTypes
}

const Email = injectIntl(withFormApi(Email_))

export default Email;