import React from 'react';
import PropTypes from 'prop-types'
import ReactDatePicker from 'react-datepicker';
import moment from 'moment'
import momentIt from 'moment/locale/it'; // NON RIMUOVERE! Serve per l'i18n di react-datepicker
import 'react-datepicker/dist/react-datepicker.css';
import {injectIntl} from 'react-intl';

import {CustomField} from "../custom-field";

class DatePicker extends CustomField {

    render = () => {
        const {placeholder, showYearDropdown, minDate, maxDate} = this.props

        let {value} = this.getFieldValues(),
            className = ''

        if (value) {
            value = moment(value)
        }
        if (this.props.className) {
            className = className + ' ' + this.props.className
        }
        return (
            <div>
                <ReactDatePicker
                    selected={value || null}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                    locale="it"
                    className={className}
                    placeholderText={this.formatMessage(placeholder)}
                    minDate={minDate}
                    maxDate={maxDate}
                    showYearDropdown={showYearDropdown}
                    scrollableYearDropdown
                    yearDropdownItemNumber={100}
                />
                {this.renderValidationMessages()}
            </div>
        )
    }
}

DatePicker.propTypes = {
    ...CustomField.propTypes,
    showYearDropdown: PropTypes.bool,
    maxDate:          PropTypes.object
};

DatePicker.defaultProps = {
    showYearDropdown: false,
    validate:         value => ({
        error:   value ? null : "#i18n:Registration.validation.notEmpty#",
        success: !!value
    })
};

export default injectIntl(DatePicker);
