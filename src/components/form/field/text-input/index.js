import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Text} from 'react-form';
import Error from "./../error";
import {withFormApi} from 'react-form';
import {FormattedMessage, injectIntl} from 'react-intl';

export class TextInput_ extends Component {

    constructor () {
        super();

        this.state = {
            errors: [],
            asyncErrors: [],
            note: ''
        }
    }

    onChange = (value) => {
        if (this.props.onChange) {
            this.props.onChange(value)
        }
        if (!this.props.formApi.touched[this.props.name]) {
            this.props.formApi.setTouched(this.props.name)
        }
    }

    onBlur = (value) => {
        if (this.props.onBlur) {
            this.props.onBlur(value)
        }
    }

    checkEmpty = (value, errors) => {
        let valid = !!value
        if (!valid) {
            errors.push(this.formatMessage("#i18n:Registration.validation.notEmpty#"))
        }
        return valid
    }

    validate = (value) => {

        let errors = [];

        this.checkEmpty(value, errors)

        this.setState({
            errors: errors
        })

        return this.state.errors.length
        ? { error: this.state.errors.join(', '), success: null }
        : { error: null, success: 'ok' }
    };

    getErrorBlock = () => {
        let rows = [];
        for(let i = 0; i < this.state.errors.length; i++) {
            rows.push(<Error error={this.state.errors[i]} key={i}/>);
        }

        return rows;
    }

    getStatusClass = () => {
        let name                  = this.props.name,
            statusClass           = 'form-group',
            {errors, asyncErrors} = this.state,
            valid                 = (errors.length + asyncErrors.length) === 0
        if (this.props.validate && this.props.formApi.touched[name]) {
            statusClass = 'form-group ' + (valid ? 'valid' : 'not-valid')
        }

        return statusClass;
    }

    getNote = () => (
        this.state.note.length
            ?  <span className="note"><FormattedMessage id={this.state.note}/></span>
            : null
        )


    getType = () => (
        'text'
    )

    formatMessage = (id) => {
        return id && this.props.intl.formatMessage({id: id})
    }

    renderField = () => {
        let {name, label, labelClass, placeholder, validate, formApi, ...rest} = this.props

        return (
            <div className={this.getStatusClass()}>
                <label className={labelClass} htmlFor={name}><FormattedMessage id={label}/></label>
                <Text
                    {...rest}
                    field={name}
                    id={name}
                    type={this.getType()}
                    placeholder={this.formatMessage(placeholder)}
                    className="form-control"
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                    validate={validate && this.validate}
                    asyncValidate={this.asyncValidate}
                />
                {this.getNote()}
                {this.getErrorBlock()}
            </div>
        )
    }

    render = () => (
        this.renderField()
     )
}

TextInput_.propTypes = {
    name:        PropTypes.string,
    placeholder: PropTypes.string,
    onChange:    PropTypes.func,
    onBlur:      PropTypes.func,
    tabIndex:    PropTypes.string,
    disabled:    PropTypes.bool,
    label:       PropTypes.string,
    labelClass:  PropTypes.string,
    validate:    PropTypes.bool, // set to false to disable validation
}

TextInput_.defaultProps = {
    validate: true
}

const TextInput = injectIntl(withFormApi(TextInput_))

export default TextInput;