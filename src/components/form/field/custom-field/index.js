import React from 'react';
import PropTypes from 'prop-types';
import {Field} from 'react-form';
import Error from "../error";
import {injectIntl} from 'react-intl';

export class CustomField extends Field {

    constructor() {
        super();

        this.onChange = this.onChange.bind(this);
        this.onBlur   = this.onBlur.bind(this);
    }

    onChange(selected) {
        const {onChange} = this.props
        // console.log("CustomField_ onchange", selected)
        this.fieldApi.setValue(selected)
        if (onChange) {
            onChange(selected)
        }
    }

    onBlur(e) {
        const {onBlur} = this.props
        this.fieldApi.setTouched()
        if (onBlur) {
            onBlur(e)
        }
    }

    formatMessage(id) {
        return id && this.props.intl.formatMessage({id: id});
    }

    renderValidationMessages = () => {
        const {error} = this.getFieldValues()
        return error && <Error error={this.formatMessage(error)}/>
    }
}

CustomField.propTypes = {
    field:       PropTypes.string.isRequired,
    validate:    PropTypes.func,
    placeholder: PropTypes.string,
    onChange:    PropTypes.func,
    onBlur:      PropTypes.func,
    className:   PropTypes.string
};

CustomField.defaultProps = {
    validate: value => ({
        error:   value ? null : "#i18n:Registration.validation.notEmpty#",
        success: !!value
    })
};

export default injectIntl(CustomField);
