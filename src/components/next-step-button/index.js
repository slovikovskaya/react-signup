import React, {Component} from 'react';
import PropTypes from "prop-types";
import {withFormApi} from 'react-form';
import {injectIntl} from 'react-intl';

class NextStepButton extends Component {
    render = () =>
        this.props.show && <input
            onClick={()=>{if (this.props.onClick) this.props.onClick()}}
            type="submit"
            className="btn btn-danger"
            value={this.props.intl.formatMessage({id: "#i18n:Registration.Confirm#"})}
            disabled={!!this.props.formApi.errors || this.props.formApi.submitting}
        />
}

NextStepButton.propTypes = {
    show: PropTypes.bool,
    onClick: PropTypes.func
};

NextStepButton.defaultProps = {
    show: true
};

export default injectIntl(withFormApi(NextStepButton))