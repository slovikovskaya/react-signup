import React, {Component} from 'react'
import {withFormApi} from 'react-form'
import {injectIntl} from 'react-intl'

class SaveButton extends Component {
    render =() => (
        <input
            id="registration_four_confirm"
            type="submit"
            className="btn btn-danger"
            value={this.props.intl.formatMessage({id: "#i18n:Registration.Save.Button#"})}
            disabled={!!this.props.formApi.errors || this.props.formApi.submitting}
        />
    )
}

export default injectIntl(withFormApi(SaveButton))