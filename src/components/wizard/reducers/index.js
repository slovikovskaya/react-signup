import {PROFILE_SELECTED, NEXT_STEP, PREV_STEP, USER_GROUP_ID_SELECTED} from '../actions'
import {
    DOCENTE_PROFILE,
    DOCENTE_UNI_PROFILE,
    STUDENTE_PROFILE,
    STUDENTE_MINORENNE_PROFILE,
    STUDENTE_UNI_PROFILE,
    PROFESSIONISTA_PROFILE
} from '../../../constants/profiles'
import {
    STEP_START,
    STEP_TIPO,
    STEP_PROVINCIA_SCUOLA,
    STEP_CLASSE_CONCORSO,
    STEP_ATENEO,
    STEP_PRIVACY,
    STEP_MATERIA_ATENEO,
    STEP_PROFESSION,
    STEP_MINORENNE
} from '../../../constants/steps'
import {
    GROUP_DOCENTE_SCOLASTICO, GROUP_DOCENTE_UNIVERSITARIO,
    GROUP_PROFESSIONISTA,
    GROUP_STUDENTE_SCOLASTICO,
    GROUP_STUDENTE_UNIVERSITARIO
} from "../../../constants/user-groups";
import {SELECT_STUDENT_TYPE} from "../../studente/studente-tipo/actions";
import {
    TIPO_STUDENTE_MINORENNE,
    TIPO_STUDENTE_MAGGIORENNE,
    TIPO_STUDENTE_UNIVERSITARIO
} from "../../../constants/studente-types";
import {DOCENTE, STUDENTE, PROFESSIONISTA} from "../../../constants/user-types";

const initialState = {
    step:        STEP_START,
    profile:     null,
    userGroupId: null
}

let STATE_MAP = {
    [STUDENTE_PROFILE]:           {
        [STEP_START]:  STEP_TIPO,
        [STEP_TIPO]:   STEP_PRIVACY
    },
    [STUDENTE_MINORENNE_PROFILE]: {
        [STEP_START]:     STEP_TIPO,
        [STEP_TIPO]:      STEP_MINORENNE,
        [STEP_MINORENNE]: STEP_PRIVACY
    },
    [STUDENTE_UNI_PROFILE]:       {
        [STEP_START]:  STEP_TIPO,
        [STEP_TIPO]:   STEP_ATENEO,
        [STEP_ATENEO]: STEP_PRIVACY
    },
    [DOCENTE_PROFILE]:            {
        [STEP_START]:            STEP_TIPO,
        [STEP_TIPO]:             STEP_PROVINCIA_SCUOLA,
        [STEP_PROVINCIA_SCUOLA]: STEP_CLASSE_CONCORSO,
        [STEP_CLASSE_CONCORSO]:  STEP_PRIVACY
    },
    [DOCENTE_UNI_PROFILE]:        {
        [STEP_START]:          STEP_TIPO,
        [STEP_TIPO]:           STEP_MATERIA_ATENEO,
        [STEP_MATERIA_ATENEO]: STEP_PRIVACY
    },
    [PROFESSIONISTA_PROFILE]:     {
        [STEP_START]:      STEP_PROFESSION,
        [STEP_PROFESSION]: STEP_PRIVACY
    }
}

let STUDENTE_TIPO_PROFILE_MAP = {
    [TIPO_STUDENTE_MINORENNE]:     STUDENTE_MINORENNE_PROFILE,
    [TIPO_STUDENTE_MAGGIORENNE]:   STUDENTE_PROFILE,
    [TIPO_STUDENTE_UNIVERSITARIO]: STUDENTE_UNI_PROFILE
}

const wizard = (state = initialState, action) => {

    if (state.profile == null && action.type === NEXT_STEP) {
        return state;
    }

    switch (action.type) {
        case PROFILE_SELECTED:
            let profile = null;
            switch(action.value){
                case DOCENTE:
                    profile = DOCENTE_PROFILE;
                    break;
                case STUDENTE:
                    profile = STUDENTE_PROFILE;
                    break;
                case PROFESSIONISTA:
                    profile = PROFESSIONISTA_PROFILE;
                    break;
                default:
                    break;
            }
            return {
                ...state,
                profile:     profile,
                userGroupId: profile === PROFESSIONISTA_PROFILE
                                 ? GROUP_PROFESSIONISTA
                                 : state.userGroupId
            }
        case NEXT_STEP:
            return {
                ...state,
                step: STATE_MAP[state.profile][state.step]
            }
        case PREV_STEP:
            return {
                ...state,
                step: Object.entries(STATE_MAP[state.profile]).find(([prev, next]) => next === state.step)[0],
            }
        case USER_GROUP_ID_SELECTED:
            let userGroupId = action.value
            return {
                ...state,
                userGroupId: userGroupId,
                profile:     userGroupId === GROUP_DOCENTE_SCOLASTICO
                                 ? DOCENTE_PROFILE
                                 : (userGroupId === GROUP_DOCENTE_UNIVERSITARIO
                                        ? DOCENTE_UNI_PROFILE
                                        : state.profile
                                    )
            }
        case SELECT_STUDENT_TYPE:
            return {
                ...state,
                profile:     STUDENTE_TIPO_PROFILE_MAP[action.value] || state.profile,
                userGroupId: action.value === TIPO_STUDENTE_UNIVERSITARIO
                                 ? GROUP_STUDENTE_UNIVERSITARIO
                                 : GROUP_STUDENTE_SCOLASTICO
            }
        default:
            return state;
    }
};

export default wizard;