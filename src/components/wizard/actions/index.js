export const NEXT_STEP              = 'NEXT_STEP'
export const PREV_STEP              = 'PREV_STEP'
export const PROFILE_SELECTED       = 'PROFILE_SELECTED'
export const USER_GROUP_ID_SELECTED = 'USER_GROUP_ID_SELECTED'

export const nextStep = () => ({
    type: NEXT_STEP
});

export const prevStep = () => ({
    type: PREV_STEP
});

export const selectProfile = (profileId) => ({
    type:  PROFILE_SELECTED,
    value: profileId
});

export const selectUserGroupId = (userGroupId) => ({
    type:  USER_GROUP_ID_SELECTED,
    value: userGroupId
});