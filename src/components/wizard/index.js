import React, { Component } from 'react'
import { Form } from 'react-form';
import PropTypes from "prop-types";
import {connect} from 'react-redux'
import {selectSchoolName, selectDistrictName} from '../docente/docente-scuola/actions/index'
import axios from 'axios'
import {FormattedMessage} from 'react-intl';

import {
    STEP_PRIVACY,
} from '../../constants/steps'

import CommonFields from "../common-fields";
import ProfileFields from "../profile-fields";
import { nextStep, selectProfile } from "./actions";
import config from "../../config";
import Summary from "../summary";
import userType, {DOCENTE} from '../../constants/user-types'

import ErrorSection from "../error-section";


class ConnectedWizard extends Component {
    constructor() {
        super();

        this.state = {
            done: false,
            serverError: false,
            email: null
        }

        this.nextStep = this.nextStep.bind(this);
    }

    save = (values) => {

        let registrationForm = Object.entries(values).map(([key, val]) => {
            return key + '=' + ((typeof val  === 'object') ? val.value : val)
        });

        registrationForm.push('user_type_radio=' + userType[this.props.userGroupId]);

        axios({
            url: config['baseUrl'] + 'ajax.php?pageId=registration&ajaxTarget=Page&action=Subscribe',
            method: 'post',
            headers: {'Accept': 'application/json'},
            data: {registrationForm: registrationForm.join('&')},
            withCredentials: true

        }).then((response) => {

            console.log("save response", response.data)

            if(200 === response.status && response.data && response.data.result && true === response.data.result.success) {

                if(userType[this.props.userGroupId] === DOCENTE) {
                    window.location = config['baseUrl'] + 'registrazione/notConfirmedTeacher';
                } else {
                    window.scrollTo(0, 0);

                    this.setState({
                        done: true,
                        email: response.data.result.loginId
                    })
                }

            } else {

                window.scrollTo(0, 0);

                this.setState({
                    serverError: true
                })
            }

        })
    }

    nextStep = (values) => {

        if(this.props.step === STEP_PRIVACY) {

            this.save(values);

        } else {

            console.log('wizard component next step', values);

            this.props.nextStep();
        }
    }

    render() {

        if(this.state.done) {
            return (
                <div id="registration_intro">
                    <h1 id="registrion_one_title"><FormattedMessage id="#i18n:Registration.Title#"/></h1>
                    <Summary email={this.state.email}/>
                </div>
            )
        }

        return (
            <Form onSubmit={this.nextStep}>
                {formApi => (
                    <div>
                        <div id="registration_intro">
                            <h1 id="registrion_one_title">
                                <FormattedMessage id="#i18n:Registration.Title#"/>
                            </h1>
                            <ErrorSection show={this.state.serverError}/>
                            <div className="col-sm-6 mark">
                                <a href="/privacy#informativa" target="_blank"><FormattedMessage
                                    id="#i18n:Registration.InformativaPrivacy#"/></a>
                                <a href="/condizioni-uso" target="_blank"><FormattedMessage
                                    id="#i18n:Registration.CondizioniUso#"/></a>
                            </div>
                            <div className="col-sm-6 mark mark--black mark--align-right">
                                <FormattedMessage id="#i18n:Registration.Required#"/>
                            </div>
                        </div>
                        <form onSubmit={formApi.submitForm}
                              id="registration_two_form"
                              className="form-register"
                              method="POST"
                              autoComplete="off"
                        >
                            <CommonFields step={this.props.step} selectProfile={this.props.selectProfile}/>
                            <ProfileFields
                                step={this.props.step}
                                profile={this.props.profile}
                                userGroupId={this.props.userGroupId}
                                schoolType={this.props.schoolType}
                                districtName={this.props.districtName}
                                selectSchoolName={this.props.selectSchoolName}
                                selectDistrictName={this.props.selectDistrictName}
                            />
                        </form>
                        {
                            <pre>
                                {JSON.stringify(
                                    {
                                        validating: formApi.validating,
                                        values: formApi.values,
                                        errors: formApi.errors
                                    }, true, 2)}
                            </pre>

                        }
                    </div>
                )
                }
            </Form>
        )
    }
}

const mapStateToProps = state => {
    return {
        profile: state.wizard.profile,
        userGroupId: state.wizard.userGroupId,
        step: state.wizard.step,
        schoolType: state.schoolType.schoolType,
        districtName: state.schoolProps.districtName
    };
};

const mapDispatchToProps = dispatch => {
    return {
        selectProfile: profile => dispatch(selectProfile(profile)),
        nextStep: () => dispatch(nextStep()),
        selectSchoolName: (schoolName) => dispatch(selectSchoolName(schoolName)),
        selectDistrictName: (districtName) => dispatch(selectDistrictName(districtName))
    };
};

const Wizard = connect(mapStateToProps, mapDispatchToProps)(ConnectedWizard);

ConnectedWizard.propTypes = {
    nextStep: PropTypes.func.isRequired
};

export default Wizard;