import React, {Component} from 'react'
import {connect} from "react-redux";
import {withFormApi} from 'react-form'
import moment from 'moment'

import {
    GROUP_DOCENTE_SCOLASTICO,
    GROUP_DOCENTE_UNIVERSITARIO,
    GROUP_STUDENTE_SCOLASTICO,
    GROUP_STUDENTE_UNIVERSITARIO,
    GROUP_PROFESSIONISTA
} from '../../constants/user-groups'
import {
    STEP_PROVINCIA_SCUOLA,
    STEP_CLASSE_CONCORSO,
    STEP_PRIVACY,
    STEP_MATERIA_ATENEO,
    STEP_MINORENNE,
    STEP_ATENEO,
    STEP_PROFESSION,
} from '../../constants/steps'
import {SECONDARY_SCHOOL_I, SECONDARY_SCHOOL_II, UNIVERSITY_TYPE, OTHER_SCHOOL_TYPE} from '../../constants/school-types'

import Step from './step'
import {
    TIPO_STUDENTE_MAGGIORENNE,
    TIPO_STUDENTE_MINORENNE,
    TIPO_STUDENTE_UNIVERSITARIO
} from "../../constants/studente-types";

class Breadcrumbs extends Component {

    constructor(props) {
        super(props);
        this.breadcrumbsSelectClassForTeacher = this.breadcrumbsSelectClassForTeacher.bind(this);
        this.breadcrumbsSelectCityForTeacher  = this.breadcrumbsSelectCityForTeacher.bind(this);
        this.breadcrumbsMateriaAteneo         = this.breadcrumbsMateriaAteneo.bind(this);
        this.breadcrumbsTeacherPrivacy        = this.breadcrumbsTeacherPrivacy.bind(this);
    }

    breadcrumbsSelectClassForTeacher(schoolType, schoolName) {
        let stringSchoolType = this.getSchoolTypeString(schoolType),
            {values}         = this.props.formApi
        if (values.school_location) {
            schoolName = values.school_name + ' - ' + values.school_location.label;
        }
        return (
            <div>
                <Step title={stringSchoolType} prev={2} label="#i18n:Registration.ChangeSchoolType#"
                      onClick={this.props.resetErrorSchoolDistrict}/>
                <Step title={schoolName} label="#i18n:Registration.SchoolEdit#" className="school-step"
                      onClick={this.props.resetErrorClasseConcorso}/>
            </div>
        )
    }

    breadcrumbsMateriaAteneo() {
        return (
            <Step title="#i18n:Registration.University#" label="#i18n:Registration.ChangeSchoolType#" onClick={this.props.resetErrorMateriaAteneo}/>
        )
    }

    breadcrumbsSelectCityForTeacher(schoolType) {
        return (
            <Step title={this.getSchoolTypeString(schoolType)} label="#i18n:Registration.ChangeSchoolType#"
                  onClick={this.props.resetErrorSchoolDistrict}/>
        );
    }

    breadcrumbsTeacherPrivacy(schoolType, schoolName) {
        let {values} = this.props.formApi
        if(values.school_location){
            schoolName = values.school_name + ' - ' + values.school_location.label;
        }
        return (
            <div>
                <Step title={this.getSchoolTypeString(schoolType)} prev={schoolType === UNIVERSITY_TYPE ? 2 : 3}
                      label="#i18n:Registration.ChangeSchoolType#" onClick={this.props.resetErrorSchoolDistrict} />

                <Step title={schoolName} prev={schoolType === UNIVERSITY_TYPE ? 1 : 2}
                      onClick={schoolType === UNIVERSITY_TYPE
                          ? this.props.resetErrorPrivacy
                          : this.props.resetErrorClasseConcorso}
                      label={schoolType === UNIVERSITY_TYPE
                          ? "#i18n:Registration.ChangeUniversitySubjectType#"
                          : "#i18n:Registration.SchoolEdit#"} />

                {schoolType !== UNIVERSITY_TYPE
                    ? <Step title={values.classe_concorso.label}
                            label="#i18n:Registration.SubjectEdit#" onClick={this.props.resetErrorPrivacy}/>
                    : null
                }

            </div>
        )
    }

    getSchoolTypeString(schoolType) {
        switch (schoolType) {
            case SECONDARY_SCHOOL_I:
                return "#i18n:Registration.FirstGrade#";
            case SECONDARY_SCHOOL_II:
                return "#i18n:Registration.SecondGrade#";
            case UNIVERSITY_TYPE:
                return "#i18n:Registration.University#";
            case OTHER_SCHOOL_TYPE:
                return "#i18n:Registration.OtherSchool#";
            default:
                return "#i18n:Registration.SchoolType.Error#";
        }
    }

    breadcrumbsMinorenne() {
        return (
            <Step title="#i18n:Registration.HighSchoolStudentUnder16#" label="#i18n:Registration.ChangeStudentType#"
                  onClick={this.props.resetErrorBirthDate}/>
        )
    }

    breadcrumbsStudenteUniversitario() {
        return (
            <Step title="#i18n:Registration.UniversitaryStudent#" label="#i18n:Registration.ChangeStudentType#"
                  onClick={this.props.resetStudentErrorAteneo}/>
        )
    }

    breadcrumbsStudentPrivacy() {
        let profile,
            prev,
            formValues   = this.props.formApi.values,
            studenteTipo = formValues['studente_tipo']

        switch (studenteTipo) {
            case TIPO_STUDENTE_MINORENNE:
                profile = "#i18n:Registration.HighSchoolStudentUnder16#";
                prev    = 2;
                break;
            case TIPO_STUDENTE_MAGGIORENNE:
                profile = "#i18n:Registration.HighSchoolStudentOver16#";
                prev    = 1;
                break;
            case TIPO_STUDENTE_UNIVERSITARIO:
                profile = "#i18n:Registration.UniversitaryStudent#";
                prev    = 2;
                break;
            default:
                return null
        }

        return (
            <div>
                <Step title={profile} prev={prev} label="#i18n:Registration.ChangeStudentType#"
                      onClick={studenteTipo === TIPO_STUDENTE_UNIVERSITARIO
                          ? this.props.resetStudentErrorAteneo
                          : (
                              studenteTipo === TIPO_STUDENTE_MAGGIORENNE
                                  ? this.props.resetStudenteMaggiorenneErrorPrivacy
                                  : this.props.resetErrorBirthDate
                          )}/>
                {studenteTipo === TIPO_STUDENTE_MINORENNE &&
                <Step title={moment(formValues['student_birth_date']).format('LL')}
                      label="#i18n:Registration.ModifyBirthDate.Label#" onClick={this.props.resetErrorPrivacyMinorenne}/>}
                {studenteTipo === TIPO_STUDENTE_UNIVERSITARIO &&
                <Step title={formValues['university_name_stud'].label}
                      label="#i18n:Registration.UniversityEdit#" onClick={this.props.resetStudentErrorPrivacy}/>}
            </div>
        )

    }

    breadcrumbsProfessione() {
        return (
            <Step title="#i18n:Registration.Professional#"/>
        )
    }

    breadcrumbsProfessionistaPrivacy() {
        return (
            <div>
                {this.breadcrumbsProfessione()}
                <Step title={this.props.formApi.values['other_professionType'].label}
                      label="#i18n:Registration.ProfessionEdit#" onClick={this.props.resetProfession}/>
            </div>
        )
    }

    render = () => {

        let {userGroupId, step, schoolType, schoolName} = this.props;

        switch (userGroupId) {
            case GROUP_DOCENTE_SCOLASTICO:
                switch (step) {
                    case STEP_PROVINCIA_SCUOLA:
                        return this.breadcrumbsSelectCityForTeacher(schoolType);
                    case STEP_CLASSE_CONCORSO:
                        return this.breadcrumbsSelectClassForTeacher(schoolType, schoolName);
                    case STEP_PRIVACY:
                        return this.breadcrumbsTeacherPrivacy(schoolType, schoolName);
                    default:
                        return null;
                }
            case GROUP_DOCENTE_UNIVERSITARIO:
                switch (step) {
                    case STEP_MATERIA_ATENEO:
                        return this.breadcrumbsMateriaAteneo();
                    case STEP_PRIVACY:
                        return this.breadcrumbsTeacherPrivacy(schoolType, this.props.formApi.values['university_name'].label);
                    default:
                        return null;
                }
            case GROUP_STUDENTE_SCOLASTICO:
                switch (step) {
                    case STEP_MINORENNE:
                        return this.breadcrumbsMinorenne();
                    case STEP_PRIVACY:
                        return this.breadcrumbsStudentPrivacy();
                    default:
                        return null;
                }
            case GROUP_STUDENTE_UNIVERSITARIO:
                switch (step) {
                    case STEP_ATENEO:
                        return this.breadcrumbsStudenteUniversitario();
                    case STEP_PRIVACY:
                        return this.breadcrumbsStudentPrivacy();
                    default:
                        return null;
                }
            case GROUP_PROFESSIONISTA:
                switch (step) {
                    case STEP_PROFESSION:
                        return this.breadcrumbsProfessione();
                    case STEP_PRIVACY:
                        return this.breadcrumbsProfessionistaPrivacy();
                    default:
                        return null;
                }
            default:
                return 'BREADCRUMBS NON ANCORA IMPLEMENTATE';
        }
    }
}

function mapStateToProps(state) {
    return {
        userGroupId: state.wizard.userGroupId,
        step:        state.wizard.step,
        schoolType:  state.schoolType.schoolType,
        schoolName:  state.schoolProps.schoolName
    };
}

export default withFormApi(connect(mapStateToProps, null)(Breadcrumbs));