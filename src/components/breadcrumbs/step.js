import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {injectIntl, FormattedMessage} from 'react-intl';

import {prevStep} from "../wizard/actions";

class Step extends Component {

    constructor() {
        super();

        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        let i = 0
        for (; i < this.props.prev; i++) {
            this.props.prevStep();
        }
        if (this.props.onClick) {
            this.props.onClick(e)
        }
    }

    shortenString(string, limit = 40) {
        if (typeof string === "string" && string.length > limit) {
            string = string.substr(0, limit) + "…";
        }
        return string;
    };

    render() {
        let {title, label} = this.props

        let className = 'step done';
        if (this.props.className) {
            className = className + ' ' + this.props.className
        }
        return (
            <div className={className}>
                <div className="title-holder">
                    <h2>{this.shortenString(this.props.intl.formatMessage({id: title}))}</h2>
                    {label &&
                    <a className="btn btn-small btn-default" onClick={this.onClick}><FormattedMessage id={label}/></a>}
                </div>
            </div>
        )
    }
}

Step.propTypes = {
    className: PropTypes.string,
    title:     PropTypes.string.isRequired,
    label:     PropTypes.string,
    onClick:   PropTypes.func,
    prev:      PropTypes.number,
}

Step.defaultProps = {
    stepClass: '',
    prev:      1
}

function mapDispatchToProps(dispatch) {
    return {
        prevStep: () => dispatch(prevStep())
    }
}

export default injectIntl(connect(null, mapDispatchToProps)(Step));