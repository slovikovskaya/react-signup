import React, {Component} from 'react'

import {
    DOCENTE_PROFILE,
    DOCENTE_UNI_PROFILE,
    STUDENTE_PROFILE,
    STUDENTE_UNI_PROFILE,
    PROFESSIONISTA_PROFILE,
    STUDENTE_MINORENNE_PROFILE
} from '../../constants/profiles'
import {STEP_START} from '../../constants/steps'

import StepIndex from './../step-index'
import Docente from './../docente'
import Studente from './../studente'
import Professionista from './../professionista'

class ProfileFields extends Component {

    render = () => {

        let {step}   = this.props
        let userForm = null

        if (step === STEP_START) {
            return null
        }

        switch (this.props.profile) {
            case DOCENTE_PROFILE:
            case DOCENTE_UNI_PROFILE:
                userForm = <Docente step={step}
                                    schoolType={this.props.schoolType}
                                    districtName={this.props.districtName}
                                    selectSchoolName={this.props.selectSchoolName}
                                    selectDistrictName={this.props.selectDistrictName}/>
                break;
            case STUDENTE_MINORENNE_PROFILE:
            case STUDENTE_PROFILE:
            case STUDENTE_UNI_PROFILE:
                userForm = <Studente step={step} />
                break;
            case PROFESSIONISTA_PROFILE:
                userForm = <Professionista step={step}/>
                break;
            default:
                return null;
        }

        return (
            <div className="steps-section">
                <StepIndex/>
                {userForm}
            </div>
        )
    }
}

export default ProfileFields;