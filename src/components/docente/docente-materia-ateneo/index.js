import React, {Component} from 'react'
import MateriaTelefono from '../docente-materia-telefono/index'
import DocenteAteneo from '../docente-ateneo/index'
import TextAreaInput  from '../../form/field/textarea-input/index'
import Breadcrumbs from '../../breadcrumbs/index'
import NextStepButton from '../../next-step-button'

export default class DocenteMateriaAteneo extends Component {
    render = () => {
        return (
            <div className="steps-bloks">

                <Breadcrumbs resetErrorMateriaAteneo={this.props.resetErrorMateriaAteneo}/>

                <div className="step active">
                    <div className="step-holder">
                        <MateriaTelefono/>
                        <DocenteAteneo ateneoDistrict={this.props.ateneoDistrict} />
                        <TextAreaInput name="university_zaninfo"
                                       label="#i18n:Registration.University.qualiMaterialiInteressato.label#"
                                       placeholder="#i18n:Registration.University.qualiMaterialiInteressato.label.clean#"
                        />
                        <TextAreaInput name="university_zaninfoconsiglia"
                                       label="#i18n:Registration.University.qualiTestiConsiglia.label#"
                                       placeholder="#i18n:Registration.University.qualiTestiConsiglia.label.clean#"
                        />
                    </div>
                </div>
                <NextStepButton/>
            </div>
        )
    }
}