import React, {Component} from 'react'
import Popup from '../../popup/index'
import PopupContent from '../../popup/common-content'
import Autocomplete from '../../form/field/select-input/autocomplete'
import axios from 'axios'
import config from '../../../config/index'
import {withFormApi} from 'react-form'
import {FormattedMessage} from 'react-intl'

class DocenteProvincia extends Component {

    constructor(props){
        super(props)
        this.state = {showSchoolSection: false}

        this.onDistrictChange = this.onDistrictChange.bind(this);
    }

    searchDistrict(input){
        if (!input || input.length < 2){
            return Promise.resolve({ options: [] });
        }

        return axios({
            url:             config['baseUrl'] + 'ajax.php?pageId=registration&ajaxTarget=Page&action=SearchDistrict',
            method:          'get',
            params: {
                term: input,
                page_limit: 10
            },
            headers:         {
                'Accept': 'application/json'
            },
            responseType:    'json',
            withCredentials: true
        }).then((response) => {
            return {options: response.data}
        })
    }

    onDistrictChange(district){
        if(district != null){
            this.props.selectDistrictName(district.value);
            //this.props.enableSchoolSection()
            this.setState({showSchoolSection: true})
            this.props.formApi.reset("user_school")
            this.props.formApi.reset("school_location")
        }
    }

    render = () => {
        return (
            <div id="user_district_div" className="form-group">
                <h2><FormattedMessage id="#i18n:Registration.DistrictNote#"/></h2>
                <em className="note"><FormattedMessage id="#i18n:Registration.DistrictNoteEm#"/></em>
                <div className="question-text">
                    <span><FormattedMessage id="#i18n:Registration.NotItalyTeacher#"/>&nbsp;</span>
                    <Popup>
                        <PopupContent/>
                    </Popup>
                </div>
                <Autocomplete
                    field="user_district"
                    className="only-search-icon"
                    getOptions={this.searchDistrict}
                    placeholder="#i18n:Registration.District#"
                    onChange={this.onDistrictChange} />
                <input type="hidden" name="user_country" value="ITALIA"/>
            </div>
        )
    }
}

export default withFormApi(DocenteProvincia);