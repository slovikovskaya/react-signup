import React, {Component} from 'react'
import {STEP_TIPO, STEP_PROVINCIA_SCUOLA, STEP_CLASSE_CONCORSO, STEP_MATERIA_ATENEO, STEP_PRIVACY} from '../../constants/steps'
import DocenteTipo from './docente-tipo/index'
import DocenteProvinciaScuola from "./docente-provincia-scuola";
import DocenteClasseConcorso from "./docente-classe-concorso";
import DocenteMateriaAteneo from "./docente-materia-ateneo";
import Privacy from "./../privacy";
import {withFormApi} from 'react-form'

class Docente extends Component {

    constructor(props){
        super(props);

        this.setSchoolManually = this.setSchoolManually.bind(this);
        this.resetProfile = this.resetProfile.bind(this);
        this.resetErrorSchoolDistrict = this.resetErrorSchoolDistrict.bind(this);
        this.resetErrorClasseConcorso = this.resetErrorClasseConcorso.bind(this);
        this.resetErrorPrivacy = this.resetErrorPrivacy.bind(this);
        this.resetErrorMateriaAteneo = this.resetErrorMateriaAteneo.bind(this);
    }

    setSchoolManually(){
        this.props.formApi.setError("user_district", null);
        this.props.formApi.reset("user_school", null);
        this.resetErrorClasseConcorso();
    }

    resetProfile(){
        this.props.formApi.reset("user_district");
        this.props.formApi.reset("user_school");
        this.props.formApi.reset("school_name");
        this.props.formApi.reset("school_location");
        this.props.formApi.reset("classe_concorso");
        this.props.formApi.reset("docente_di_sostegno");

        this.props.formApi.reset("univ_subject");
        this.props.formApi.reset("user_phone");
        this.props.formApi.reset("user_district_4");
        this.props.formApi.reset("university_name");
        this.props.formApi.reset("university_department");
        this.props.formApi.reset("university_faculty");
        this.props.formApi.reset("university_course");
        this.props.formApi.reset("university_teaching");
        this.props.formApi.reset("university_role");
        this.props.formApi.reset("university_coursestart");
        this.props.formApi.reset("university_courseyear");
        this.props.formApi.reset("university_student");
        this.props.formApi.reset("university_zaninfo");
        this.props.formApi.reset("university_zaninfoconsiglia");
    }

    resetErrorSchoolDistrict(){
        this.props.formApi.reset("school_type_radio");
        this.props.formApi.setError("user_district", null);
        this.props.formApi.setError("user_school", null);
        this.props.formApi.reset("school_name");
        this.props.formApi.reset("school_location");
        this.resetErrorClasseConcorso();
    }

    resetErrorClasseConcorso(){
        this.props.formApi.setError("classe_concorso", null);
        this.resetErrorPrivacy();
    }

    resetErrorMateriaAteneo(){
        this.props.formApi.reset("school_type_radio");
        this.props.formApi.setError("user_district_4", null);
        this.props.formApi.setError("university_name", null);
        this.props.formApi.setError("university_course", null);
        this.props.formApi.setError("university_teaching", null);
        this.props.formApi.setError("university_role", null);
        this.props.formApi.setError("university_coursestart", null);
        this.props.formApi.setError("university_courseyear", null);
        this.props.formApi.setError("university_zaninfo", null);
        this.props.formApi.setError("university_zaninfoconsiglia", null);

        this.resetErrorPrivacy();
    }

    resetErrorPrivacy(){
        this.props.formApi.setError("user_wantActivation", null);
        this.props.formApi.setError("user_wantActivation2", null);
    }

    render = () => {
        switch (this.props.step) {
            case STEP_TIPO:
                return <DocenteTipo resetProfile={this.resetProfile} />
            case STEP_PROVINCIA_SCUOLA:
                return <DocenteProvinciaScuola
                            schoolType={this.props.schoolType}
                            districtName={this.props.districtName}
                            selectSchoolName={this.props.selectSchoolName}
                            selectDistrictName={this.props.selectDistrictName}
                            setSchoolManually={this.setSchoolManually}
                            resetErrorSchoolDistrict={this.resetErrorSchoolDistrict} />
            case STEP_CLASSE_CONCORSO:
                return <DocenteClasseConcorso resetErrorSchoolDistrict={this.resetErrorSchoolDistrict}
                                              resetErrorClasseConcorso={this.resetErrorClasseConcorso}/>
            case STEP_MATERIA_ATENEO:
                return <DocenteMateriaAteneo resetErrorMateriaAteneo={this.resetErrorMateriaAteneo}
                                             ateneoDistrict={this.props.formApi.values.user_district_4}/>
            case STEP_PRIVACY:
                return <Privacy resetErrorSchoolDistrict={this.resetErrorSchoolDistrict}
                                resetErrorClasseConcorso={this.resetErrorClasseConcorso}
                                resetErrorPrivacy={this.resetErrorPrivacy}/>
            default:
                return null;
        }
    }
}

export default withFormApi(Docente);