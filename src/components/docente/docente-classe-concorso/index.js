import React, {Component} from 'react'
import Breadcrumbs from '../../breadcrumbs/index'
import Autocomplete from '../../form/field/select-input/autocomplete'
import CheckboxInput from "../../form/field/checkbox-input/index"
import NextStepButton from '../../next-step-button'
import axios from 'axios'
import config from '../../../config/index'
import {FormattedMessage} from 'react-intl'

export default class DocenteClasseConcorso extends Component {

    searchClasseConcorso(input){
        if (!input || input.length < 2){
            return Promise.resolve({ options: [] });
        }

        return axios({
            url:             config['baseUrl'] + 'rest/classiconcorso/search',
            method:          'get',
            params: {
                term: input,
                page_limit: 10
            },
            headers:         {
                'Accept': 'application/json'
            },
            responseType:    'json',
            withCredentials: true
        }).then((response) => {
            return {options: response.data.classiConcorso}
        })
    }

    render = () => {

        return (

            <div className="steps-bloks">
                <Breadcrumbs resetErrorSchoolDistrict={this.props.resetErrorSchoolDistrict}
                             resetErrorClasseConcorso={this.props.resetErrorClasseConcorso} />
                <div id="subject_panel" className="step active">
                        <div id="user_highschool_subject" className="step-holder">
                            <h2><FormattedMessage id="#i18n:Registration.ClasseConcorso#"/></h2>
                            <em className="note"><FormattedMessage id="#i18n:Registration.ClasseConcorsoNoteEm#"/></em>
                            <Autocomplete
                                field="classe_concorso"
                                className="only-search-icon selectized"
                                getOptions={this.searchClasseConcorso}
                                placeholder="#i18n:Registration.ClasseConcorso#"
                                tabIndex="9" />
                            <div className="checkbox no-margin">
                                <CheckboxInput
                                    name="docente_di_sostegno"
                                    label="#i18n:Registration.DocenteDiSostegno.title#" />
                            </div>
                        </div>
                    <NextStepButton/>
                </div>
            </div>
        )
    }
}