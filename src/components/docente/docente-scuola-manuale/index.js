import React, {Component} from 'react'
import Autocomplete from '../../form/field/select-input/autocomplete'
import axios from 'axios'
import config from '../../../config/index'
import {SECONDARY_SCHOOL_I, SECONDARY_SCHOOL_II, OTHER_SCHOOL_TYPE} from '../../../constants/school-types'
import TextInput from "../../form/field/text-input/index";
import {withFormApi} from 'react-form'
import NextStepButton from '../../next-step-button'
import {FormattedMessage} from 'react-intl'

class DocenteScuolaManuale extends Component {

    constructor(props){
        super(props);

        this.searchSchoolLocation = this.searchSchoolLocation.bind(this);
        this.onSchoolLocationSelected = this.onSchoolLocationSelected.bind(this);

        switch(this.props.schoolType){
            case SECONDARY_SCHOOL_I:
                this.state = {schoolTypeRadio: 1}
                break;
            case SECONDARY_SCHOOL_II:
                this.state = {schoolTypeRadio: 2}
                break;
            case OTHER_SCHOOL_TYPE:
                this.state = {schoolTypeRadio: 4}
                break;
            default:

        }
    }

    searchSchoolLocation(input){
        if (!input || input.length < 2){
            return Promise.resolve({ options: [] });
        }

        return axios({
            url:             config['baseUrl'] + 'ajax.php?pageId=registration&ajaxTarget=Page&action=SearchSchoolLocation',
            method:          'get',
            params: {
                term: input,
                page_limit: 10,
                district: this.props.districtName,
                type: this.state.schoolTypeRadio
            },
            headers:         {
                'Accept': 'application/json'
            },
            responseType:    'json',
            withCredentials: true
        }).then((response) => {
            return {options: response.data}
        })
    }

    onSchoolLocationSelected(input){
        console.log(input)
    }

    render = () => {

        return (
                <div>
                    <h2><FormattedMessage id="#i18n:Registration.SchoolManualHeader#"/></h2>
                    <em className="note"/>
                    <div className="form-group">
                        <TextInput name="school_name" label="#i18n:Registration.SchoolName#" labelClass="no-responsive" placeholder="#i18n:Registration.SchoolName.Placeholder#" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="school_location"><FormattedMessage id="#i18n:Registration.SchoolLocation#"/></label>
                        <Autocomplete
                            field="school_location"
                            className="only-search-icon"
                            getOptions={this.searchSchoolLocation}
                            placeholder="#i18n:Registration.SchoolLocation.Placeholder#"
                            onChange={this.onSchoolLocationSelected}
                            disabled={!this.props.schoolSectionEnabled}/>
                   </div>
                    <NextStepButton/>
                </div>
        )
    }
}

export default withFormApi(DocenteScuolaManuale);