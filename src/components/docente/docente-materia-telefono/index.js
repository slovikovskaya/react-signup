import React, {Component} from 'react'
import SelectAsync from '../../form/field/select-input/async'
import TextInput from '../../form/field/text-input/index'
import config from "../../../config";
import axios from "axios/index";
import {FormattedMessage} from 'react-intl'

export default class Materia extends Component {

    getMaterie = () => {
        return axios({
            url: config['baseUrl'] + 'rest/materie',
            method: 'get',
            headers: {'Accept': 'application/json'},
            responseType:    'json',
            withCredentials: true

        }).then((response) => {
            let options;
            if (200 === response.status) {
                options = response.data.content
            } else {
                options = []
            }

            return {
                options: options.map( (item) => ({value: item.code, label: item.descr}) )
            }
        })
    }

    render = () => {

        return (
            <div>
                <h2><FormattedMessage id="#i18n:Registration.Subject#"/></h2>
                <div className="input-holder">
                    <div className="form-group not-show-icon">
                        <SelectAsync
                            field="univ_subject"
                            className="form-control"
                            getOptions={this.getMaterie}
                            placeholder="#i18n:Registration.select.noChoice#"
                        />
                    </div>
                    <div className="form-group">
                        <TextInput name="user_phone" className="form-control" validate={false}
                                   label="#i18n:Registration.Phone#"
                                   placeholder="#i18n:Registration.Phone#"/>
                    </div>
                </div>
            </div>
        )
    }
}