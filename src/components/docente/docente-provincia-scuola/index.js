import React, {Component} from 'react'
import Breadcrumbs from '../../breadcrumbs/index'
import DocenteProvincia from '../docente-provincia/index'
import DocenteScuola from "../docente-scuola/index";
import DocenteScuolaManuale from "../docente-scuola-manuale/index";
import {OTHER_SCHOOL_TYPE} from '../../../constants/school-types'
import {withFormApi} from 'react-form'

class DocenteProvinciaScuola extends Component {

    constructor(props){
        super(props)
        this.state = { schoolManual: false};

        this.setSchoolManually = this.setSchoolManually.bind(this);
    }

    setSchoolManually(){
        this.setState({schoolManual: true})
        this.props.setSchoolManually();
    }

    render = () => {
        return (
            <div className="steps-bloks">
                <Breadcrumbs resetErrorSchoolDistrict={this.props.resetErrorSchoolDistrict}/>
                <div id="school_panel" className="step active">
                    <div className="step-holder">
                        <DocenteProvincia selectDistrictName={this.props.selectDistrictName}
                                          resetSchool={this.props.resetSchool}m/>
                        { (this.state.schoolManual || this.props.schoolType  === OTHER_SCHOOL_TYPE ?
                            <DocenteScuolaManuale
                                districtName={this.props.districtName}
                                schoolType={this.props.schoolType}
                                selectSchoolName={this.props.selectSchoolName}
                                schoolSectionEnabled={!!this.props.formApi.values.user_district} /> :
                            <DocenteScuola setSchoolManually={this.setSchoolManually}
                                           schoolType={this.props.schoolType}
                                           districtName={this.props.districtName}
                                           selectSchoolName={this.props.selectSchoolName}
                                           schoolSectionEnabled={!!this.props.formApi.values.user_district}/>) }

                    </div>
                </div>
            </div>
        )
    }

}

export default withFormApi(DocenteProvinciaScuola);