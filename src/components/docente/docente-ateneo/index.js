import React, {Component} from 'react'
import Autocomplete from '../../form/field/select-input/autocomplete'
import SelectInput from '../../form/field/select-input'
import SelectAsync from '../../form/field/select-input/async'
import TextInput from '../../form/field/text-input/index'
import {UNIVERSITY_COURSE_YEARS} from '../../../constants/university-course-years'
import {getProvinceAtenei, getAtenei} from '../../ateneo-helper/index'
import config from "../../../config";
import axios from "axios/index";
import {withFormApi} from 'react-form'
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';

class DocenteAteneo extends Component {
    constructor() {
        super();

        this.state = {
            provincia: null
        }

        this.getProvinceAtenei     = getProvinceAtenei.bind(this)
        this.getAtenei             = getAtenei.bind(this)
        this.getRuoli              = this.getRuoli.bind(this)
        this.onChangeProvinciaAteneo = this.onChangeProvinciaAteneo.bind(this);
    }

    onChangeProvinciaAteneo(value){
        this.props.formApi.reset("university_name");
        this.setState({provincia: value});
    }

    getRuoli = () => {
        return axios({
            url:             config['baseUrl'] + 'rest/getUniversityTeacherRoles',
            method:          'get',
            headers:         {'Accept': 'application/json'},
            responseType:    'json',
            withCredentials: true

        }).then((response) => {
            let options;
            if (200 === response.status) {
                options = response.data
            } else {
                options = []
            }

            return {
                options: options.map((item) => ({value: item.id, label: item.nome}))
            }
        })
    }

    render = () => {
        return (
            <div>
                <h2><FormattedMessage id="#i18n:Registration.UniversityNote#"/></h2>
                <div>
                    <h2><FormattedMessage id="#i18n:Registration.UniversityDistrictNote#"/></h2>

                    <em className="note"><FormattedMessage id="#i18n:Registration.DistrictNoteEm#"/></em>
                    <em className="note">
                        <FormattedHTMLMessage id="#i18n:Registration.DistrictNoteUniEm#"/>
                    </em>
                    <div id="user_district_4_div"
                         className="form-group university-teacher input-search form-selectiz not-show-icon">
                        <label>
                        </label>
                        <Autocomplete
                            field="user_district_4"
                            className="university-teacher only-search-icon"
                            getOptions={this.getProvinceAtenei}
                            placeholder="#i18n:Registration.UniversityDistrict#"
                            onChange={this.onChangeProvinciaAteneo}
                        />
                    </div>
                    <div id="university_name_div" className="form-group university-field input-search not-show-icon">
                        <label htmlFor="university_name"><FormattedMessage id="#i18n:Registration.UniversityName#"/></label>
                        <em className="note"><FormattedMessage id="#i18n:Registration.UniversityName#"/></em>
                        <Autocomplete
                            field="university_name"
                            className="university-teacher only-search-icon"
                            getOptions={this.getAtenei}
                            placeholder="#i18n:Registration.UniversityNamePlaceHolder#"
                            disabled={!this.props.ateneoDistrict}
                        />
                    </div>
                    <div className="form-group">
                        <TextInput name="university_department" className="form-control" validate={false}
                                   label="#i18n:Registration.UniversityDepartment#"
                                   placeholder="#i18n:Registration.UniversityDepartment#"/>
                    </div>
                    <div className="form-group">
                        <TextInput name="university_faculty" className="form-control" validate={false}
                                   label="#i18n:Registration.UniversityFaculty#"
                                   placeholder="#i18n:Registration.UniversityFaculty#"/>
                    </div>
                    <TextInput name="university_course" label="#i18n:Registration.UniversityCourse#" labelClass="no-responsive"
                               placeholder="#i18n:Registration.UniversityCoursePlaceHolder#"/>
                    <TextInput name="university_teaching" label="#i18n:Registration.UniversityTeaching#"
                               placeholder="#i18n:Registration.UniversityTeachingPlaceHolder#"/>
                    <div className="form-group">
                        <SelectAsync
                            field="university_role"
                            className="form-control"
                            getOptions={this.getRuoli}
                            placeholder="#i18n:Registration.select.noChoice#"
                            label="#i18n:Registration.UniversityRole#"
                        />
                    </div>
                    <TextInput name="university_coursestart" label="#i18n:Registration.UniversityCourseStart#"
                               placeholder="#i18n:Registration.UniversityCourseStartPlaceHolder#"/>
                    <div className="form-group">
                        <SelectInput
                            field="university_courseyear"
                            options={UNIVERSITY_COURSE_YEARS}
                            className="form-control"
                            placeholder="#i18n:Registration.select.noChoice#"
                            label="#i18n:Registration.UniversityCourseYear#"
                        />
                    </div>
                    <div className="form-group">
                        <TextInput name="university_student" className="form-control" validate={false}
                                   label="#i18n:Registration.UniversityStudent#"
                                   placeholder="#i18n:Registration.UniversityStudent#"/>
                    </div>
                </div>
            </div>
        )
    }
}

export default withFormApi(DocenteAteneo);