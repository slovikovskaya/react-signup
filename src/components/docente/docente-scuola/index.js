import React, {Component} from 'react'
import Autocomplete from '../../form/field/select-input/autocomplete'
import axios from 'axios'
import NextStepButton from '../../next-step-button'
import config from '../../../config/index'
import {SECONDARY_SCHOOL_I, SECONDARY_SCHOOL_II, OTHER_SCHOOL_TYPE} from '../../../constants/school-types'
import {FormattedMessage} from 'react-intl'

export default class DocenteScuola extends Component {

    constructor(props){
        super(props)

        this.searchSchool = this.searchSchool.bind(this);
        this.onSchoolChange = this.onSchoolChange.bind(this);

        switch(this.props.schoolType){
            case SECONDARY_SCHOOL_I:
                this.state = {schoolTypeRadio: 1}
                break;
            case SECONDARY_SCHOOL_II:
                this.state = {schoolTypeRadio: 2}
                break;
            case OTHER_SCHOOL_TYPE:
                this.state = {schoolTypeRadio: 4}
                break;
            default:
        }
    }

    searchSchool(input){
        if (!input || input.length < 2){
            return Promise.resolve({ options: [] });
        }

        return axios({
            url:             config['baseUrl'] + 'ajax.php?pageId=registration&ajaxTarget=Page&action=SearchSchool',
            method:          'get',
            params: {
                term: input,
                page_limit: 10,
                district: this.props.districtName,
                type: this.state.schoolTypeRadio
            },
            headers:         {
                'Accept': 'application/json'
            },
            responseType:    'json',
            withCredentials: true
        }).then((response) => {
            return {options: response.data}
        })
    }

    onSchoolChange(school){
        if(school != null){
            this.props.selectSchoolName(school.label)
        }
    }

    render = () => {
        return (
                <div id="user_shool_div">
                    <div id="user_school_select_div" className="js-userSchoolValidationRoW">
                        <h2 id="user_school_label"><FormattedMessage id="#i18n:Registration.School#"/></h2>
                        <em className="note"><FormattedMessage id="#i18n:Registration.SchoolNote#"/></em>
                        <Autocomplete
                            field="user_school"
                            className="only-search-icon"
                            getOptions={this.searchSchool}
                            placeholder="#i18n:Registration.SchoolName.Placeholder#"
                            onChange={this.onSchoolChange}
                            disabled={!this.props.schoolSectionEnabled}/>
                        <div className="block-school-choose">
                        </div>
                    </div>
                    <div id="user_school_manual_div" className="button-block text-center">
                        <p><FormattedMessage id="#i18n:Registration.Otherwise#"/></p>
                        <a id="registration_school_manual"
                           className="btn btn-default"
                           onClick={this.props.setSchoolManually}>
                            <FormattedMessage id="#i18n:Registration.SchoolManual#"/>
                        </a>
                    </div>
                    <NextStepButton/>
                </div>
        )
    }
}