export const SCHOOL_NAME_SELECTED = 'SCHOOL_NAME_SELECTED'
export const DISTRICT_NAME_SELECTED = 'DISTRICT_NAME_SELECTED'
export const LOCATION_NAME_SELECTED = 'LOCATION_NAME_SELECTED'

export const selectSchoolName = (schoolName) => {
    return {
        type:  SCHOOL_NAME_SELECTED,
        value: schoolName
    };
}

export const selectDistrictName = (districtName) => {
    return {
        type:  DISTRICT_NAME_SELECTED,
        value: districtName
    };
}

export const selectLocationName = (locationName) => {
    return {
        type:  LOCATION_NAME_SELECTED,
        value: locationName
    };
}
