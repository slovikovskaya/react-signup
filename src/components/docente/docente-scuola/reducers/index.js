import {SCHOOL_NAME_SELECTED, DISTRICT_NAME_SELECTED} from '../actions/index'

const initialState = {
    schoolName: null,
    districtName: null
}

const schoolProps = (state = initialState, action) => {
    switch (action.type) {
        case SCHOOL_NAME_SELECTED:
            let schoolName = action.value
            return {
                ...state,
                schoolName: schoolName
            }
        case DISTRICT_NAME_SELECTED:
            let districtName = action.value
            return {
                ...state,
                districtName: districtName
            }
        default:
            return state;
    }
};

export default schoolProps;