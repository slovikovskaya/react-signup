import {SCHOOL_TYPE_SELECTED} from '../actions/index'

const initialState = {
    schoolType: null
}

const schoolType = (state = initialState, action) => {
    switch (action.type) {
        case SCHOOL_TYPE_SELECTED:
            let schoolType = action.value
            return {
                ...state,
                schoolType: schoolType
            }
        default:
            return state;
    }
};

export default schoolType;