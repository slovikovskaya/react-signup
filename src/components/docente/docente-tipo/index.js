import React, {Component} from 'react'
import {Radio} from 'react-form'
import RadioGroupInput from '../../form/field/radio-group-input/index'
import {SECONDARY_SCHOOL_I, SECONDARY_SCHOOL_II, UNIVERSITY_TYPE, OTHER_SCHOOL_TYPE} from '../../../constants/school-types'
import {connect} from "react-redux";
import {GROUP_DOCENTE_SCOLASTICO, GROUP_DOCENTE_UNIVERSITARIO} from '../../../constants/user-groups'
import {nextStep, selectUserGroupId} from '../../wizard/actions/index'
import {selectSchoolType} from './actions/index'
import {FormattedMessage} from 'react-intl'

class DocenteTipo extends Component {

    constructor(){
        super()
        this.changeSchoolType = this.changeSchoolType.bind(this)
    }

    changeSchoolType = (event) => {

        if(this.props.schoolType !== event.target.value && this.props.schoolType !=null){
            this.props.resetProfile();
        }

        switch (event.target.value){
            case SECONDARY_SCHOOL_I:
                this.props.selectUserGroupId(GROUP_DOCENTE_SCOLASTICO);
                this.props.selectSchoolType(SECONDARY_SCHOOL_I)
                break;
            case SECONDARY_SCHOOL_II:
                this.props.selectUserGroupId(GROUP_DOCENTE_SCOLASTICO);
                this.props.selectSchoolType(SECONDARY_SCHOOL_II)
                break;
            case UNIVERSITY_TYPE:
                this.props.selectUserGroupId(GROUP_DOCENTE_UNIVERSITARIO);
                this.props.selectSchoolType(UNIVERSITY_TYPE)
                break;
            case OTHER_SCHOOL_TYPE:
                this.props.selectUserGroupId(GROUP_DOCENTE_SCOLASTICO);
                this.props.selectSchoolType(OTHER_SCHOOL_TYPE)
                break;
            default:
        }

        this.props.nextStep()
    }

    render = () => {

        return (

            <div id="school_type" className="steps-bloks">
                <div className="step active">
                    <div className="step-holder">

                        <RadioGroupInput name="school_type_radio" onChange={this.changeSchoolType}>
                            <div className="radio">
                                <Radio id="kind_firstGrade" value={SECONDARY_SCHOOL_I}/>
                                <label htmlFor="kind_firstGrade"><FormattedMessage id="#i18n:Registration.FirstGrade#"/></label>
                            </div>
                            <div className="radio">
                                <Radio id="kind_secondGrade" value={SECONDARY_SCHOOL_II}/>
                                <label htmlFor="kind_secondGrade"><FormattedMessage id="#i18n:Registration.SecondGrade#"/></label>
                            </div>
                            <div className="radio">
                                <Radio id="kind_university" value={UNIVERSITY_TYPE}/>
                                <label htmlFor="kind_university"><FormattedMessage id="#i18n:Registration.University#"/></label>
                            </div>
                            <div className="radio">
                                <Radio id="kind_otherschool" value={OTHER_SCHOOL_TYPE}/>
                                <label htmlFor="kind_otherschool"><FormattedMessage id="#i18n:Registration.OtherSchool#"/></label>
                            </div>
                        </RadioGroupInput>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        schoolType: state.schoolType.schoolType
    };
};

const mapDispatchToProps = dispatch => {
    return {
        nextStep: () => dispatch(nextStep()),
        selectUserGroupId: (userGroupId) => dispatch(selectUserGroupId(userGroupId)),
        selectSchoolType: (schoolType) => dispatch(selectSchoolType(schoolType))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DocenteTipo);