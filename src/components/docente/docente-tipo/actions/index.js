export const SCHOOL_TYPE_SELECTED = 'SCHOOL_TYPE_SELECTED'

export const selectSchoolType = (schoolType) => {
    return {
        type:  SCHOOL_TYPE_SELECTED,
        value: schoolType
    };
}