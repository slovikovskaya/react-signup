import React, {Component} from 'react';
import {FormattedMessage} from 'react-intl';

export default class ErrorSection extends Component {
    render = () => (
            this.props.show ?
                <div className="error-section-2">
                    <div className="error-block">
                        <div className="error-message-large have-small-icon">
                            <p id="idErrorMessage" className="text-danger"><FormattedMessage id="#i18n:Registration.Error.General.Gentle#"/></p>
                        </div>
                    </div>
                </div>
                : null
        )
}