import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';

class StepIndexItem extends Component {

    render = () => {
        let classes   = [],
            className = null
        if (this.props.isActive) {
            classes.push("active")
        }
        if (this.props.isDone) {
            classes.push("done")
        }
        if (classes.length) {
            className = classes.join(" ")
        }
        return (
            <li className={className}>{this.props.count+1} <FormattedMessage id={this.props.title}/></li>
        )
    }
}

StepIndexItem.propTypes = {
    isActive: PropTypes.bool,
    isDone:   PropTypes.bool,
    title:    PropTypes.string.isRequired,
    count:    PropTypes.number
};

export default StepIndexItem;

