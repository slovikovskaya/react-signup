import React, {Component} from 'react'
import {connect} from "react-redux";
import {FormattedMessage} from 'react-intl';

import {
    STEPS_DOCENTE_SCOLASTICO,
    STEPS_STUDENTE_MINORENNNE,
    STEPS_STUDENTE_SCOLASTICO,
    STEPS_STUDENTE_UNIVERSITARIO,
    STEPS_PROFESSIONISTA,
    STEPS_DOCENTE_UNIVERSITARIO
} from '../../constants/steps'
import {
    DOCENTE_PROFILE,
    DOCENTE_UNI_PROFILE,
    STUDENTE_PROFILE,
    STUDENTE_UNI_PROFILE,
    PROFESSIONISTA_PROFILE,
    STUDENTE_MINORENNE_PROFILE
} from '../../constants/profiles'

import StepIndexItem from './item'

class StepIndex extends Component {

    renderStepItems = (items) => {
        let currentIndex = items.findIndex(item => item.step === this.props.step)
        return (
            <ol className="step-list list-unstyled">
                {items.map((item, count) => (
                        <StepIndexItem key={item.step} isActive={currentIndex === count} isDone={currentIndex > count}
                                       count={count} title={item.title}/>
                    )
                )}
            </ol>
        )
    }

    render = () => {

        switch (this.props.profile) {
            case DOCENTE_PROFILE:
                return this.renderStepItems(STEPS_DOCENTE_SCOLASTICO)
            case DOCENTE_UNI_PROFILE:
                return this.renderStepItems(STEPS_DOCENTE_UNIVERSITARIO)
            case STUDENTE_MINORENNE_PROFILE:
                return this.renderStepItems(STEPS_STUDENTE_MINORENNNE)
            case STUDENTE_PROFILE:
                return this.renderStepItems(STEPS_STUDENTE_SCOLASTICO)
            case STUDENTE_UNI_PROFILE:
                return this.renderStepItems(STEPS_STUDENTE_UNIVERSITARIO)
            case PROFESSIONISTA_PROFILE:
                return this.renderStepItems(STEPS_PROFESSIONISTA)
            default:
                return (
                    <ol className="step-list list-unstyled">
                        <li id="menu_school_3" className="active">1 <FormattedMessage id="#i18n:Registration.Professione#"/></li>
                        <li className="active">2 <FormattedMessage id="#i18n:Registration.CondizioniUso#"/></li>
                    </ol>
                )
        }
    }
}

function mapStateToProps(state) {
    return {
        step:    state.wizard.step,
        profile: state.wizard.profile
    };
}

export default connect(mapStateToProps)(StepIndex);