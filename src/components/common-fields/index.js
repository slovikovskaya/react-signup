import React, {Component} from 'react'
import {DOCENTE_PROFILE, STUDENTE_PROFILE, PROFESSIONISTA_PROFILE} from "../../constants/profiles"
import { Radio, withFormApi } from 'react-form';
import TextInput from "./../form/field/text-input";
import Email from "./../form/field/email";
import Password from "./../form/field/password";
import PasswordConfirm from "./../form/field/password-confirm";
import RadioGroupInput from "../form/field/radio-group-input";
import NextStepButton from "../next-step-button";
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';

import {
    STEP_START,
} from '../../constants/steps'
import PropTypes from "prop-types";

class CommonFields extends Component {

    constructor(props){
        super(props)
        this.state = {step: STEP_START};
    }

    changeProfile = (e) => {
        this.props.selectProfile(e.target.value);
    }

    // FIXME: DEBUG ONLY
    componentDidMount = () => {
        this.props.formApi.setAllValues({
            "user_firstName": "Mario",
            "user_lastName": "rossi",
            "user_email": "mario"+ Math.round(1000 * Math.random()) +"@rossi.it",
            "user_password": "password",
            "user_password_cfm": "password"
        })
    }

    render = () => {
        return (
            <div id="registration_one">
                <div className="row">
                    <div className="col-sm-4">
                        <span className="text-large label-text"><FormattedHTMLMessage id="#i18n:Registration.Iam#"/></span>
                        <RadioGroupInput name="user_type_radio" onChange={this.changeProfile} tabIndex="1" disabled={this.props.step !== STEP_START}>
                             <div className="radio-block">
                                <div className="radio">
                                    <Radio name="user_type_radio" id={DOCENTE_PROFILE} value="1" disabled={this.props.step !== STEP_START}/>
                                    <label htmlFor={DOCENTE_PROFILE} className="text-primary"><FormattedMessage id="#i18n:Registration.Teacher#"/></label>
                                </div>
                                <div className="radio">
                                    <Radio name="user_type_radio" id={STUDENTE_PROFILE} value="2" disabled={this.props.step !== STEP_START}/>
                                    <label htmlFor={STUDENTE_PROFILE} className="text-success"><FormattedMessage id="#i18n:Registration.Student#"/></label>
                                </div>
                                <div className="radio">
                                    <Radio name="user_type_radio" id={PROFESSIONISTA_PROFILE} value="3" disabled={this.props.step !== STEP_START}/>
                                    <label htmlFor={PROFESSIONISTA_PROFILE}><FormattedMessage id="#i18n:Registration.Parent#"/>, <br/><FormattedHTMLMessage id="#i18n:Registration.Other#"/></label>
                                </div>
                            </div>
                        </RadioGroupInput>
                    </div>
                </div>
                <div id="registration_one_inputs" className="row">
                    <div className="col-sm-8">
                        <div className="row">
                            <div className="col-sm-6">
                                <TextInput name="user_firstName" label="#i18n:Registration.Name#" tabIndex="2" />
                             </div>
                            <div className="col-sm-6">
                                <TextInput name="user_lastName" label="#i18n:Registration.Lastname#" tabIndex="3" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6">
                                <Password name="user_password" label="#i18n:Registration.Password#" tabIndex="5"/>
                            </div>
                            <div className="col-sm-6">
                                <PasswordConfirm name="user_password_cfm" label="#i18n:Registration.ConfirmPassword#" tabIndex="6"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <Email name="user_email" label="#i18n:Registration.Email#" tabIndex="4"/>
                    </div>
                </div>
                <NextStepButton show={this.props.step === STEP_START} />
            </div>
        )
    }
}

CommonFields.propTypes = {
    selectProfile: PropTypes.func.isRequired
};

export default withFormApi(CommonFields)