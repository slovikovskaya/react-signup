import {SELECT_STUDENT_TYPE} from '../actions/index'

const initialState = {
    type: null
}

const studentType = (state = initialState, action) => {
    switch (action.type) {
        case SELECT_STUDENT_TYPE:
            return {
                type : action.value
            }
        default:
            return state;
    }
};

export default studentType;