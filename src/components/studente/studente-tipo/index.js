import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Radio, RadioGroup, withFormApi} from 'react-form';
import {FormattedMessage} from 'react-intl';
import {selectStudentType} from './actions/index';

import {
    TIPO_STUDENTE_MINORENNE,
    TIPO_STUDENTE_MAGGIORENNE,
    TIPO_STUDENTE_UNIVERSITARIO
} from '../../../constants/studente-types'

class StudenteTipo extends Component {

    changeStudentType = (e) => {
        if(this.props.studentType !== e.target.value){
            this.props.resetFields();
        }
        this.props.selectStudentType(e.target.value);
        this.props.formApi.submitForm(this.props.formApi.values)
    }

    render = () => {

        return (
            <div id="student_type" className="steps-bloks">
                <div className="step active">
                    <div className="step-holder">
                        <RadioGroup field="studente_tipo" onChange={this.changeStudentType}>
                            <div className="radio">
                                <Radio name="studente_tipo" id={TIPO_STUDENTE_MINORENNE} value={TIPO_STUDENTE_MINORENNE}/>
                                <label htmlFor={TIPO_STUDENTE_MINORENNE}><FormattedMessage id="#i18n:Registration.HighSchoolStudentUnder16#"/></label>
                            </div>
                            <div className="radio">
                                <Radio name="studente_tipo" id={TIPO_STUDENTE_MAGGIORENNE} value={TIPO_STUDENTE_MAGGIORENNE}/>
                                <label htmlFor={TIPO_STUDENTE_MAGGIORENNE}><FormattedMessage id="#i18n:Registration.HighSchoolStudentOver16#"/></label>
                            </div>
                            <div className="radio">
                                <Radio name="studente_tipo" id={TIPO_STUDENTE_UNIVERSITARIO} value={TIPO_STUDENTE_UNIVERSITARIO}/>
                                <label htmlFor={TIPO_STUDENTE_UNIVERSITARIO}><FormattedMessage id="#i18n:Registration.UniversitaryStudent#"/></label>
                            </div>
                        </RadioGroup>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        selectStudentType: type => dispatch(selectStudentType(type))
    };
};

const mapStateToProps = state => {
    return {
        studentType: state.studentType.type
    };
};

export default withFormApi(connect(mapStateToProps, mapDispatchToProps)(StudenteTipo));