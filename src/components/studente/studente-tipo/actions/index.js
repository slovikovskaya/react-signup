export const SELECT_STUDENT_TYPE = 'SELECT_STUDENT_TYPE'

export const selectStudentType = (type) => ({
    type:  SELECT_STUDENT_TYPE,
    value: type
});