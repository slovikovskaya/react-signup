import React, {Component} from 'react'
import DatePicker from '../../form/field/datepicker'
import moment from 'moment'
import Breadcrumbs from '../../breadcrumbs/index'
import NextStepButton from "../../next-step-button/index";
import {FormattedMessage} from 'react-intl';

export default class StudenteMinorenne extends Component {
    render = () => {
        return (
            <div className="steps-bloks">
                <Breadcrumbs resetErrorBirthDate={this.props.resetErrorBirthDate}/>
                <div className="step active">
                    <div className="step-holder">
                        <div className="form-group">
                            <h2><FormattedMessage id="#i18n:Registration.BirthDate.Label#"/></h2>
                            <DatePicker field="student_birth_date"
                                        className="form-control"
                                        showYearDropdown
                                        placeholder="#i18n:Registration.BirthDate.Placeholder#"
                                        minDate={moment().subtract(16, 'years')}
                                        maxDate={moment()}
                            />
                        </div>
                        <NextStepButton/>
                    </div>
                </div>
            </div>
        )
    }
}










