import React, {Component} from 'react'
import {
    STEP_TIPO,
    STEP_ATENEO,
    STEP_PRIVACY,
    STEP_MINORENNE
} from "../../constants/steps";
import StudenteTipo from './studente-tipo'
import StudenteAteneo from './studente-ateneo'
import StudenteMinorenne from "./studente-minorenne";
import Privacy from "./../privacy";
import {withFormApi} from 'react-form'

class Studente extends Component {

    constructor(props){
        super(props);

        this.resetFields = this.resetFields.bind(this);
        this.resetStudentErrorAteneo = this.resetStudentErrorAteneo.bind(this);
        this.resetStudentErrorPrivacy = this.resetStudentErrorPrivacy.bind(this);
        this.resetStudenteMaggiorenneErrorPrivacy = this.resetStudenteMaggiorenneErrorPrivacy.bind(this);
        this.resetErrorBirthDate = this.resetErrorBirthDate.bind(this);
        this.resetErrorPrivacyMinorenne = this.resetErrorPrivacyMinorenne.bind(this);
    }

    resetFields(){
        this.props.formApi.reset('user_district_unistud');
        this.props.formApi.reset('university_name_stud');
        this.props.formApi.reset('university_course_stud');
        this.props.formApi.reset("student_birth_date");
        this.props.formApi.reset("student_parent_email");
        this.props.formApi.reset("user_young_privacy");
        this.props.formApi.reset('user_wantActivation');
        this.props.formApi.reset('user_wantActivation2');
    }

    resetStudentErrorAteneo(){
        this.props.formApi.reset('studente_tipo');
        this.props.formApi.setError('user_district_unistud', null);
        this.props.formApi.setError('university_name_stud', null);
        this.props.formApi.setError('university_course_stud', null);
        this.resetStudentErrorPrivacy();
    }

    resetStudentErrorPrivacy(){
        this.props.formApi.setError('user_wantActivation', null);
        this.props.formApi.setError('user_wantActivation2', null);
    }

    resetStudenteMaggiorenneErrorPrivacy(){
        this.props.formApi.reset('studente_tipo');
        this.resetStudentErrorPrivacy();
    }

    resetErrorBirthDate(){
        this.props.formApi.reset('studente_tipo');
        this.props.formApi.reset("student_birth_date");
        this.resetErrorPrivacyMinorenne();
    }

    resetErrorPrivacyMinorenne(){
        this.resetStudentErrorPrivacy();
        this.props.formApi.setError("user_young_privacy", null);
        this.props.formApi.setError("student_parent_email", null);
    }

    render = () => {
        switch (this.props.step) {
            case STEP_TIPO:
                return <StudenteTipo resetFields={this.resetFields}/>
            case STEP_ATENEO:
                return <StudenteAteneo resetStudentErrorAteneo={this.resetStudentErrorAteneo}/>
            case STEP_MINORENNE:
                return <StudenteMinorenne resetErrorBirthDate={this.resetErrorBirthDate}/>
            case STEP_PRIVACY:
                return <Privacy resetStudentErrorAteneo={this.resetStudentErrorAteneo}
                                resetStudentErrorPrivacy={this.resetStudentErrorPrivacy}
                                resetStudenteMaggiorenneErrorPrivacy={this.resetStudenteMaggiorenneErrorPrivacy}
                                resetErrorBirthDate={this.resetErrorBirthDate}
                                resetErrorPrivacyMinorenne={this.resetErrorPrivacyMinorenne}/>
            default:
                return null;
        }
    }
}

export default withFormApi(Studente);