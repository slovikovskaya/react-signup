import React, {Component} from 'react'
import Autocomplete from '../../form/field/select-input/autocomplete'
import TextAreaInput from '../../form/field/textarea-input'
import Popup from '../../popup'
import NextStepButton from '../../next-step-button'
import Breadcrumbs from '../../breadcrumbs'
import {getProvinceAtenei, getAtenei} from '../../ateneo-helper'
import {withFormApi} from 'react-form'
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl'

class StudenteAteneo extends Component {
    constructor () {
        super();

        this.state = {
            provincia: null
        }

        this.getProvinceAtenei = getProvinceAtenei.bind(this)
        this.getAtenei = getAtenei.bind(this)
        this.onChangeProvinciaAteneo = this.onChangeProvinciaAteneo.bind(this);
    }

    onChangeProvinciaAteneo(value){
        this.setState({provincia: value});
        this.props.formApi.reset("university_name_stud");
    }

    render = () => {
        let popupContent = (<div>
                                <FormattedHTMLMessage id="#i18n:Registration.ElencoStatiAtenei#"/>
                            </div>);

        return (
                <div className="steps-bloks">
                    <Breadcrumbs resetStudentErrorAteneo={this.props.resetStudentErrorAteneo}/>
                    <div id="school_panel" className="step active">
                        <div className="step-holder">
                            <h2><FormattedMessage id="#i18n:Registration.UniversityDistrictNote#"/></h2>
                            <em className="note"><FormattedMessage id="#i18n:Registration.DistrictNoteEm#"/></em>
                            <div className="question-text">
                                <span><FormattedMessage id="#i18n:Registration.NotItalyStudent#"/>&nbsp;</span>
                                <Popup>
                                    {popupContent}
                                </Popup>
                            </div>
                            <div id="user_district_unistud_div" className="form-group input-search form-selectiz not-show-icon">
                                <label>
                                </label>
                                <Autocomplete
                                    field="user_district_unistud"
                                    className="only-search-icon"
                                    getOptions={this.getProvinceAtenei}
                                    placeholder="#i18n:RegistrationStudent.Provincia ateneo#"
                                    onChange={this.onChangeProvinciaAteneo}
                                />
                            </div>

                            <div id="university_name_stud_div" className="form-group input-search form-selectiz not-show-icon">
                                <label htmlFor="university_name"><FormattedMessage id="#i18n:Registration.UniversityName#"/></label>
                                <em className="note"><FormattedMessage id="#i18n:Registration.UniversityNoteEm#"/></em>
                                <Autocomplete
                                    field="university_name_stud"
                                    className="only-search-icon"
                                    getOptions={this.getAtenei}
                                    placeholder="#i18n:Registration.UniversityNamePlaceHolder#"
                                    disabled={!this.props.formApi.values.user_district_unistud}
                                />
                            </div>
                            <TextAreaInput name="university_course_stud"
                                           label="#i18n:Registration.University.qualiCorsiFrequenti.label#"
                                           placeholder="#i18n:Registration.University.qualiCorsiFrequenti.placeholder#"/>
                            <NextStepButton/>
                        </div>
                    </div>
                </div>
        )
    }
}

export default withFormApi(StudenteAteneo);