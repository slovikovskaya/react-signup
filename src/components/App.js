import React from "react";
import Wizard from "./wizard";

const App = () => (
    <div className="signup-form">
        <Wizard/>
    </div>
);
export default App;