import React, {Component} from 'react'
import axios from 'axios'
import config from '../../config'
import {withFormApi} from 'react-form'

import {STEP_PRIVACY, STEP_PROFESSION} from '../../constants/steps'

import Autocomplete from '../form/field/select-input/autocomplete'
import SelectAsync from '../form/field/select-input/async'
import Popup from '../popup'
import PopupContent from '../popup/common-content'
import Breadcrumbs from '../breadcrumbs'

import NextStepButton from '../next-step-button'
import Privacy from '../privacy'
import {FormattedMessage} from 'react-intl';

class Professionista extends Component {

    constructor(props) {
        super(props)

        this.resetProfession = this.resetProfession.bind(this);
    }

    getProvincie = (input) => {
        if (!input || input.length < 2)
            return Promise.resolve({options: []});

        return axios({
            url:             config['baseUrl'] + 'ajax.php?pageId=registration&ajaxTarget=Page&action=SearchDistrict&term=' + input,
            method:          'get',
            headers:         {
                'Accept': 'application/json'
            },
            responseType:    'json',
            withCredentials: true
        })
            .then((response) => {
                return {options: response.data}
            })
    }

    resetProfession(){
        this.props.formApi.setError("other_professionType", null);
        this.props.formApi.setError("user_wantActivation2", null);
        this.props.formApi.setError("user_wantActivation", null);
    }

    getProfessioni = () => {
        return axios({
            url: config['baseUrl'] + 'rest/profession-type',
            method: 'get',
            headers: {'Accept': 'application/json'},
            responseType:    'json',
            withCredentials: true

        }).then((response) => {
            let options;
            if(200 === response.status) {
                options = response.data
            } else {
                options = []
            }

            return {options: options}

        })

    }

    renderProfessionista = () => {
        return (
            <div className="steps-bloks">
                <Breadcrumbs />
                <div id="school_panel" className="step active">
                    <div className="step-holder">
                        <h2><FormattedMessage id="#i18n:Registration.DistrictNote#"/></h2>
                        <em className="note"><FormattedMessage id="#i18n:Registration.DistrictNoteEm#"/></em>
                        <div className="question-text">
                            <span><FormattedMessage id="#i18n:Registration.NotItalyProfessional#"/>&nbsp;</span>
                            <Popup>
                                <PopupContent/>
                            </Popup>
                        </div>
                        <div id="user_district_3_div" className="form-group ">
                            <label>
                            </label>
                            <Autocomplete
                                field="user_district_3"
                                getOptions={this.getProvincie}
                                placeholder="#i18n:Registration.District#"
                            />
                        </div>
                        <div className="form-group">
                            <h2><FormattedMessage id="#i18n:Registration.ProfessionType#"/></h2>
                            <SelectAsync
                                field="other_professionType"
                                getOptions={this.getProfessioni}
                                placeholder="#i18n:Registration.Profession.Placeholder#"
                            />
                        </div>
                        <NextStepButton/>
                    </div>
                </div>
            </div>
        )

    }

    render = () => {

        switch (this.props.step) {
            case STEP_PROFESSION:
                return this.renderProfessionista()
            case STEP_PRIVACY:
                return <Privacy resetProfession={this.resetProfession}/>
            default:
                return "Step professionista imprevisto";
        }
    }
}

export default withFormApi(Professionista);