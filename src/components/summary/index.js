import React, {Component} from 'react';
import {FormattedMessage, FormattedHTMLMessage, injectIntl} from 'react-intl';

export class Summary extends Component {

    render = () => {

        const message = this.props.intl.formatMessage({id: "#i18n:Registration.awaitingConfirm.2#"}).replace("**EMAIL**", this.props.email);

        return (
            <div className="content">
                <h2 id="registration_thankyou_title"><FormattedMessage id="#i18n:Registration.ThankYou#"/></h2>
                <div id="registration_end_message">
                    <FormattedMessage id="#i18n:Registration.awaitingConfirm.1#"/>
                    {message}
                    <FormattedHTMLMessage id="#i18n:Registration.awaitingConfirm.3#"/>
                </div>
                <p id="registration_support"><FormattedHTMLMessage id="#i18n:Registration.Support#"/></p>
            </div>
        )}
}

export default injectIntl(Summary);