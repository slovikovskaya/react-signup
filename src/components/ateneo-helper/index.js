import config from "../../config";
import axios from "axios/index";

export function getProvinceAtenei (input) {
    if (!input || input.length < 2)
        return Promise.resolve({options: []});

    return axios({
        url: config['baseUrl'] + 'ajax.php?pageId=registration&ajaxTarget=Page&action=SearchUniversityDistrict',
        method: 'get',
        params: {
            term: input,
            page_limit: 10
        },
        headers: {
            'Accept': 'application/json'
        },
        responseType: 'json',
        withCredentials: true
    })
        .then((response) => {
            return {options: response.data}
        })
}

export function provinciaAteneoChange (value) {
    this.setState({provincia: value})
}

export function getAtenei (input) {
    if (!input || input.length < 2)
        return Promise.resolve({options: []});

    return axios({
        url: config['baseUrl'] + 'ajax.php?pageId=registration&ajaxTarget=Page&action=SearchUniversity',
        method: 'get',
        params: {
            term: input,
            page_limit: 10,
            district: this.state.provincia.value
        },
        headers: {
            'Accept': 'application/json'
        },
        responseType: 'json',
        withCredentials: true
    })
        .then((response) => {
            return {options: response.data}
        })
}