export const UNIVERSITY_COURSE_YEARS = [
    {
        label: 'Primo',
        value: 'primo'
    },
    {
        label: 'Secondo',
        value: 'secondo'
    },
    {
        label: 'Terzo',
        value: 'terzo'
    },
    {
        label: 'Quarto',
        value: 'quarto'
    },
    {
        label: 'Quinto',
        value: 'quinto'
    },
    {
        label: 'Sesto',
        value: 'sesto'
    }
]