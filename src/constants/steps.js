export const STEP_START            = "STEP_START"
export const STEP_TIPO             = "STEP_TIPO"
export const STEP_PROVINCIA_SCUOLA = "STEP_PROVINCIA_SCUOLA"
export const STEP_CLASSE_CONCORSO  = "STEP_CLASSE_CONCORSO"
export const STEP_MATERIA_ATENEO   = "STEP_MATERIA_ATENEO"
export const STEP_ATENEO           = "STEP_ATENEO"
export const STEP_SCUOLA           = "STEP_SCUOLA"
export const STEP_MINORENNE        = "STEP_MINORENNE"
export const STEP_PROFESSION       = "STEP_PROFESSION"
export const STEP_PRIVACY          = "STEP_PRIVACY"

export const STEPS_STUDENTE_MINORENNNE = [
    {
        step:  STEP_TIPO,
        title: "#i18n:Registration.TipoStudente#"
    },
    {
        step:  STEP_MINORENNE,
        title: "#i18n:Registration.BirthDate.Step#"
    },
    {
        step:  STEP_PRIVACY,
        title: "#i18n:Registration.CondizioniUso#"
    }
]

export const STEPS_STUDENTE_SCOLASTICO = [
    {
        step:  STEP_TIPO,
        title: "#i18n:Registration.TipoStudente#"
    },
    {
        step:  STEP_PRIVACY,
        title: "#i18n:Registration.CondizioniUso#"
    }
]

export const STEPS_STUDENTE_UNIVERSITARIO = [
    {
        step:  STEP_TIPO,
        title: "#i18n:Registration.TipoStudente#"
    },
    {
        step:  STEP_ATENEO,
        title: "#i18n:Registration.IlTuoAteneo#"
    },
    {
        step:  STEP_PRIVACY,
        title: "#i18n:Registration.CondizioniUso#"
    }
]

export const STEPS_PROFESSIONISTA = [
    {
        step:  STEP_PROFESSION,
        title: "#i18n:Registration.Professione#"
    },
    {
        step:  STEP_PRIVACY,
        title: "#i18n:Registration.CondizioniUso#"
    }
]

export const STEPS_DOCENTE_SCOLASTICO = [
    {
        step:  STEP_TIPO,
        title: "#i18n:Registration.TipoScuola#"
    },
    {
        step:  STEP_PROVINCIA_SCUOLA,
        title: "#i18n:Registration.LaTuaScuola#"
    },
    {
        step:  STEP_CLASSE_CONCORSO,
        title: "#i18n:Registration.LaTuaMateria#"
    },
    {
        step:  STEP_PRIVACY,
        title: "#i18n:Registration.CondizioniUso#"
    }
]

export const STEPS_DOCENTE_UNIVERSITARIO = [
    {
        step: STEP_TIPO,
        title: "#i18n:Registration.TipoScuola#"
    },
    {
        step: STEP_MATERIA_ATENEO,
        title: "#i18n:Registration.SubjectUniversity#"
    },
    {
        step: STEP_PRIVACY,
        title: "#i18n:Registration.CondizioniUso#"
    }
]