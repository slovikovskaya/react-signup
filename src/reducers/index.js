import {combineReducers} from 'redux';
import wizard from '../components/wizard/reducers'
import studentType from '../components/studente/studente-tipo/reducers'
import schoolType from '../components/docente/docente-tipo/reducers'
import schoolProps from '../components/docente/docente-scuola/reducers'

const rootReducer = combineReducers({
    wizard,
    studentType,
    schoolType,
    schoolProps
})

export default rootReducer;